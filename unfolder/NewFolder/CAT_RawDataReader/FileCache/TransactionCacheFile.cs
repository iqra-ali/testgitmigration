﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Globalization;
//using BusinessObjects.Helper_Classes;
//using BusinessObjects.Models;
using CAT_RawDataReader.Helper_Classes;
using CAT_RawDataReader.Models;
using System.Xml.Serialization;
using System.Xml;

namespace CAT_RawDataReader
{
    public class TransactionCacheFile : ITransactionalCache
    {
        public List<NW> lsNewORders = null;

        
        private Dictionary<string, string> _dSides;
        private Dictionary<string, string> _dTimeInForce;
        private Dictionary<string, List<string>> _dMultiValdict;
        private ConfigManager.File _File;

        public TransactionCacheFile(ConfigManager.File objectFile)
        {

            // IA - TODO: Add the following conventions in XML config too
            _dSides = new Dictionary<string, string>();
            _dMultiValdict = new Dictionary<string, List<string>>();
            _dSides.Add("B", "1");
            _dSides.Add("SL", "2");
            _dSides.Add("SE", "6");
            _dSides.Add("SSE", "7");
            _dTimeInForce = new Dictionary<string, string>();
            _dTimeInForce.Add("DAY", "0");
            _dTimeInForce.Add("GTC", "1");
            _dTimeInForce.Add("FOK", "4");
            _dMultiValdict.Add("1", new List<string>{"B", "BUY", "BU"});
            _dMultiValdict.Add("2", new List<string> {"S", "SEL", "SL", "SELL"});
            _File = objectFile;
        }

        public string getSideKeyByValue(string value)
        {
           
            foreach (var item in _dMultiValdict)
            {
                if (item.Value.Contains(value))
                {
                    return item.Key;
                }
            }
            return "Could not find given value's key";
        }

        public void LoadTransactionalData(/*ConfigManager.File file tring Path, string BoothID*/)
        {
            DeserializeFileData(/*file.FilePath*/);
            StoreParsedDataInDB(/*file*/);
        }
        private void DeserializeFileData(/*string path*/)
        {
            string[] fileRecords = File.ReadAllLines(_File.FilePath); 

            NW oNewRecord = null;
            lsNewORders = new List<NW>();
            if (fileRecords != null || fileRecords.Length > 1)
            {
                foreach (string recdord in fileRecords)
                {
                    string[] fields = recdord.Split(_File.Delimiter);
                    if (fields[0] == "NW")
                    {
                        oNewRecord = new NW(fields);
                        lsNewORders.Add(oNewRecord);
                    }
                    else if (fields[0] == "RT")
                    {
                        if (oNewRecord != null)
                            oNewRecord.FillListRT(fields);
                    }
                    else if (fields[0] == "EX")
                    {
                        if (oNewRecord != null)
                            oNewRecord.FillListEX(fields);
                    }
                }
            }
        }

        private void StoreParsedDataInDB(/*ConfigManager.File obFile*/)
        {
            DatabaseContext transactionalDBContext = new DatabaseContext();
            Orders _order = null;
            OrderDetails _orderDetail = null;
            Execution _execution = null;
            int inExecId = 1;
            string stBoothID = _File.BoothId;
            string stClientID = _File.ClientId;
            string stTrader = _File.Trader;
            string stOrderID = Helper.GetNextOrderIdByBooth(stBoothID);//GetLastOrderIdByBooth(BoothId);

            if (lsNewORders != null)
            {
                foreach (NW newOrder in lsNewORders)
                {
                    _order = new Orders();
                    _orderDetail = new OrderDetails();
                    _execution = new Execution();
                    int majorVersion = 1;

                    stOrderID = Helper.GetIcreamentedOrderId(stOrderID);

                    //__Logging
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(newOrder))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(newOrder);
                        Console.WriteLine("{0}={1}", name, value);
                    }

                    #region tableOrders 
                    _order.BoothId = stBoothID;
                    _order.OrderId = stOrderID;
                    _order.SecurityType = null;
                    _order.ClientId = stClientID;
                    _order.Symbol = newOrder.Symbol;
                    _order.Account = null;
                    _order.FutSettDate = null;
                    _order.SymbolSfx = newOrder.SymbolSuffix;
                    _order.SecurityExchange = null;
                    _order.PrevClosePx = null;
                    _order.Commision = null;
                    _order.Text = null;
                    _order.ExecBroker = null;
                    _order.ExDestination = null;
                    _order.BrokerDesc = null;
                    _order.SettlementId = "0";
                    _order.Rule80A = "A";
                    _order.ProcessCode = null;
                    _order.CommissionType = null;
                    _order.OrderCapacity2 = null;
                    _order.WriteInTime = null;
                    _order.TargetSubId = null;
                    _order.BoothOrdIdent = "B";
                    _order.SenderLocationId = null;
                    _order.TargetLocationId = stBoothID;
                    _order.SenderSubId = null;
                    _order.ExternalOrderId = null;
                    _order.LocationId = null;
                    _order.ContactName = null;
                    _order.Oatsinst = null;
                    _order.SecurityInfo = null;

                    #endregion tableOrders

                    #region tableOrderDetails 
                    _orderDetail.BoothId = stBoothID;
                    _orderDetail.OrderId = stOrderID;
                    _orderDetail.OrderMajorVersionNo = majorVersion;
                    _orderDetail.OrderMinorVersionNo = 0;
                    //_orderDetail.ExpireTime = ConvertPlainStringToDatetime(newOrder.ExpirationDate);
                    _orderDetail.OrderQty = int.Parse(newOrder.Quantity);
                    _orderDetail.MinQty = null;
                    _orderDetail.OrderType = "2";
                    _orderDetail.MaxFloor = null;
                    _orderDetail.Price = double.Parse(newOrder.LimitPrice);
                    _orderDetail.ClientOrderId = newOrder.FirmOrderID;//  "2019020517525B";
                    double stopPrice = double.TryParse(newOrder.StopPrice, out stopPrice) ? stopPrice : 0;

                    _orderDetail.StopPx = stopPrice;
                    _orderDetail.HandlInstId = "1";
                    _orderDetail.PegDifference = null;
                    _orderDetail.MaxShow = null;
                    _orderDetail.TimeInForceId = _dTimeInForce[newOrder.TimeInForce];
                    _orderDetail.CashOrderQty = null;
                    _orderDetail.LocateReqd = null;
                    _orderDetail.MessageTag = 68;
                    // IA- Need to convert into GMT (disussed internally with US and MS) after excluding miliseconds.

                    _orderDetail.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
                    _orderDetail.PossibleResend = null;
                    _orderDetail.PossibleDuplicate = null;
                    _orderDetail.SendingTime = null;
                    _orderDetail.OrigSendingTime = null;
                    _orderDetail.SideId = getSideKeyByValue(newOrder.Side);
                    _orderDetail.OrignatingUserId = "0";
                    _orderDetail.BillTo = null;
                    _orderDetail.BrokerBadgeNo = null;
                    _orderDetail.RemainingQty = null;
                    _orderDetail.ContraBroker = null;
                    _orderDetail.OrdRouteStatus = null;
                    _orderDetail.PercentageOfVolume = 0;
                    _orderDetail.SendingComputerId = null;
                    _orderDetail.TargetComputerId = null;
                    _orderDetail.OnBehalfOfComputerId = null;
                    _orderDetail.DeliverToComputerId = null;
                    _orderDetail.ClientOrderId = newOrder.FirmOrderID; //"2019020517525B"; /*newOrder.FirmOrderID;*/
                    _orderDetail.OriginatingUserDesc = stTrader;
                    _orderDetail.OrigClOrdId = null;
                    _orderDetail.DisplayQty = null; ;
                    _orderDetail.CxlType = null;
                    _orderDetail.ExecInst = null;
                    _orderDetail.OriginalOrderId = null;
                    _orderDetail.ParentOrderId = null;
                    _orderDetail.DestinationId = null;
                    _orderDetail.OrderMarker = 1;
                    _orderDetail.Accepted = "1";
                    _orderDetail.ExInst = null;
                    _orderDetail.AlgoInst = null;
                    _orderDetail.OptionsFields = null;
                    _orderDetail.ComplexFields = null;
                    _orderDetail.CrossedOrderId = null;
                    _orderDetail.ExtraFields = null;
                    _orderDetail.PairOrderId = null;
                    _orderDetail.TraderInitial = null;
                    _orderDetail.MarketData = null;
                    _orderDetail.TradingDate = null;
                    _orderDetail.LocationId = null;
                    _orderDetail.ContactName = null;
                    _orderDetail.Text = null;
                    _orderDetail.SecurityInfo = null;
                    _orderDetail.BaseCurrencyInfo = null;
                    _orderDetail.TransactTimeMicroseconds = newOrder.OrderReceivedTimestamp;

                    #endregion tableOrderDetails

                    #region tableExecution
                    _execution.ExecId = inExecId.ToString();
                    _execution.BoothId = stBoothID;
                    _execution.OrderId = stOrderID;
                    _execution.ExecType = "0";
                    _execution.MajorVersionNo = ++majorVersion;
                    _execution.MinorVersionNo = 0;
                    _execution.ExecBroker = newOrder.TradingSessionCode;
                    _execution.SymbolSfx = newOrder.SymbolSuffix;
                    _execution.SideId = getSideKeyByValue(newOrder.Side);
                    _execution.LastShares = 0;
                    _execution.ClientOrderId = newOrder.FirmOrderID;//"2019020517525B";
                    _execution.ExecNature = "1";
                    _execution.ClientId = stClientID;
                    _execution.Symbol = newOrder.Symbol;
                    _execution.OrderStatus = "0";
                    _execution.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
                    _execution.TradeDate = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd");
                    _execution.OrderQty = int.Parse(newOrder.Quantity);
                    _execution.LeavesQty = int.Parse(newOrder.Quantity);
                    //double stopPrice = double.TryParse(newOrder.StopPrice, out stopPrice) ? stopPrice : 0;
                    _execution.Price = double.Parse(newOrder.LimitPrice);
                    _execution.TimeInForceId = _dTimeInForce[newOrder.TimeInForce];
                    _execution.OrderType = "2";
                    _execution.MessageTag = 8;
                    _execution.OrignatingUserId = stTrader;
                    _execution.Rule80A = "A";
                    _execution.OrderMarker = 1;
                    _execution.OrdRouteStatus = 0;
                    _execution.TargetLocationId = stBoothID;
                    _execution.ExecRefId = "0";

                    #endregion tableExecution

                    _order.OrderDetails.Add(_orderDetail);
                    _order.Executions.Add(_execution);
                    if (newOrder.srLsttransactions != null)
                    {
                        for (int i = 1; i <= newOrder.srLsttransactions.Values.Count; i++)
                        {
                            ITran transaction = newOrder.srLsttransactions[i];
                            if (transaction is EX)
                            {
                                Execution execution = new Execution();
                                EX ex = transaction as EX;
                                execution.BoothId = stBoothID;
                                execution.OrderId = stOrderID;
                                execution.ExecType = "2";
                                //execution.MajorVersionNo = i;
                                execution.MajorVersionNo = _execution.MajorVersionNo + i;

                                // Mendatory Fields
                                execution.ExecId = (++inExecId).ToString();
                                execution.MinorVersionNo = 0;
                                execution.LastShares = ex.Quantity;
                                execution.Price = ex.Price;
                                execution.Rule80A = ex.CapacityCode;
                                execution.OrderStatus = "2";
                                execution.ClientOrderId = newOrder.FirmOrderID;//"2019020517525B";
                                execution.ClientId = stClientID;
                                execution.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
                                execution.TimeInForceId = _dTimeInForce[newOrder.TimeInForce];
                                execution.SideId = getSideKeyByValue(newOrder.Side);
                                execution.OrderType = "2";
                                execution.Rule80A = "A";
                                execution.OrignatingUserId = stTrader;
                                execution.OrderMarker = 1;
                                execution.OrdRouteStatus = 0;
                                execution.TargetLocationId = stBoothID;
                                execution.ExecRefId = "0";
                                execution.ExecNature = "1";
                                execution.Symbol = newOrder.Symbol;
                                _order.Executions.Add(execution);
                            }
                            else if (transaction is RT)
                            {
                                Console.WriteLine("Fill tables for RT Details");
                            }
                        }
                    }
                    transactionalDBContext.tb_Order.Add(_order);
                }
            }
            transactionalDBContext.SaveChanges();
        }

        //        private void StoreParsedDataInDB(/*ConfigManager.File obFile*/)
        //        {
        //            DatabaseContext transactionalDBContext = new DatabaseContext();
        //            Orders _order = null;
        //            OrderDetails _orderDetail = null;
        //            Execution _execution = null;
        //            int inExecId = 1;
        //            string stBoothID = _File.BoothId;
        //            string stClientID = _File.ClientId;
        //            string stTrader = _File.Trader;
        //            string stOrderID = Helper.GetNextOrderIdByBooth(stBoothID);//GetLastOrderIdByBooth(BoothId);

        //            if (lsNewORders != null)
        //            {
        //                foreach (NW newOrder in lsNewORders)
        //                {
        //                    _order = new Orders();
        //                    _orderDetail = new OrderDetails();
        //                    _execution = new Execution();
        //                    int majorVersion = 1;

        //                    stOrderID = Helper.GetIcreamentedOrderId(stOrderID);

        //                    //__Logging
        //                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(newOrder))
        //                    {
        //                        string name = descriptor.Name;
        //                        object value = descriptor.GetValue(newOrder);
        //                        Console.WriteLine("{0}={1}", name, value);
        //                    }
        //#region tableOrders 
        //                    _order.BoothId = stBoothID;
        //                    _order.OrderId = stOrderID;
        //                    _order.SecurityType = null;
        //                    _order.ClientId = stClientID;
        //                    _order.Symbol = newOrder.Symbol;
        //                    _order.Account = null;
        //                    _order.FutSettDate = null;
        //                    _order.SymbolSfx = newOrder.SymbolSuffix;
        //                    _order.SecurityExchange = null;
        //                    _order.PrevClosePx = null;
        //                    _order.Commision = null;
        //                    _order.Text = null;
        //                    _order.ExecBroker = null;
        //                    _order.ExDestination = null;
        //                    _order.BrokerDesc = null;
        //                    _order.SettlementId = "0";
        //                    _order.Rule80A = "A";
        //                    _order.ProcessCode = null;
        //                    _order.CommissionType = null;
        //                    _order.OrderCapacity2 = null;
        //                    _order.WriteInTime = null;
        //                    _order.TargetSubId = null;
        //                    _order.BoothOrdIdent = "B";
        //                    _order.SenderLocationId = null;
        //                    _order.TargetLocationId = stBoothID;
        //                    _order.SenderSubId = null;
        //                    _order.ExternalOrderId = null;
        //                    _order.LocationId = null;
        //                    _order.ContactName = null;
        //                    _order.Oatsinst = null;
        //                    _order.SecurityInfo = null;

        //#endregion tableOrders

        //#region tableOrderDetails 
        //                    _orderDetail.BoothId = stBoothID;
        //                    _orderDetail.OrderId = stOrderID;
        //                    _orderDetail.OrderMajorVersionNo = majorVersion;
        //                    _orderDetail.OrderMinorVersionNo = 0;
        //                    //_orderDetail.ExpireTime = ConvertPlainStringToDatetime(newOrder.ExpirationDate);
        //                    _orderDetail.OrderQty = int.Parse(newOrder.Quantity);
        //                    _orderDetail.MinQty = null;
        //                    _orderDetail.OrderType = "2";
        //                    _orderDetail.MaxFloor = null;
        //                    _orderDetail.Price = double.Parse(newOrder.LimitPrice);
        //                    _orderDetail.ClientOrderId = newOrder.FirmOrderID;//  "2019020517525B";
        //                    double stopPrice = double.TryParse(newOrder.StopPrice, out stopPrice) ? stopPrice : 0;

        //                    _orderDetail.StopPx = stopPrice;
        //                    _orderDetail.HandlInstId = "1";
        //                    _orderDetail.PegDifference = null;
        //                    _orderDetail.MaxShow = null;
        //                    _orderDetail.TimeInForceId = _dTimeInForce[newOrder.TimeInForce];
        //                    _orderDetail.CashOrderQty = null;
        //                    _orderDetail.LocateReqd = null;
        //                    _orderDetail.MessageTag = 68;
        //                    // IA- Need to convert into GMT (disussed internally with US and MS) after excluding miliseconds.

        //                    _orderDetail.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
        //                    _orderDetail.PossibleResend = null;
        //                    _orderDetail.PossibleDuplicate = null;
        //                    _orderDetail.SendingTime = null;
        //                    _orderDetail.OrigSendingTime = null;
        //                    _orderDetail.SideId = getSideKeyByValue(newOrder.Side);
        //                    _orderDetail.OrignatingUserId = "0";
        //                    _orderDetail.BillTo = null;
        //                    _orderDetail.BrokerBadgeNo = null;
        //                    _orderDetail.RemainingQty = null;
        //                    _orderDetail.ContraBroker = null;
        //                    _orderDetail.OrdRouteStatus = null;
        //                    _orderDetail.PercentageOfVolume = 0;
        //                    _orderDetail.SendingComputerId = null;
        //                    _orderDetail.TargetComputerId = null;
        //                    _orderDetail.OnBehalfOfComputerId = null;
        //                    _orderDetail.DeliverToComputerId = null;
        //                    _orderDetail.ClientOrderId = newOrder.FirmOrderID; //"2019020517525B"; /*newOrder.FirmOrderID;*/
        //                    _orderDetail.OriginatingUserDesc = stTrader;
        //                    _orderDetail.OrigClOrdId = null;
        //                    _orderDetail.DisplayQty = null; ;
        //                    _orderDetail.CxlType = null;
        //                    _orderDetail.ExecInst = null;
        //                    _orderDetail.OriginalOrderId = null;
        //                    _orderDetail.ParentOrderId = null;
        //                    _orderDetail.DestinationId = null;
        //                    _orderDetail.OrderMarker = 1;
        //                    _orderDetail.Accepted = "1";
        //                    _orderDetail.ExInst = null;
        //                    _orderDetail.AlgoInst = null;
        //                    _orderDetail.OptionsFields = null;
        //                    _orderDetail.ComplexFields = null;
        //                    _orderDetail.CrossedOrderId = null;
        //                    _orderDetail.ExtraFields = null;
        //                    _orderDetail.PairOrderId = null;
        //                    _orderDetail.TraderInitial = null;
        //                    _orderDetail.MarketData = null;
        //                    _orderDetail.TradingDate = null;
        //                    _orderDetail.LocationId = null;
        //                    _orderDetail.ContactName = null;
        //                    _orderDetail.Text = null;
        //                    _orderDetail.SecurityInfo = null;
        //                    _orderDetail.BaseCurrencyInfo = null;
        //                    _orderDetail.TransactTimeMicroseconds = newOrder.OrderReceivedTimestamp;

        //                    #endregion tableOrderDetails

        //#region tableExecution
        //                    _execution.ExecId = inExecId.ToString();
        //                    _execution.BoothId = stBoothID;
        //                    _execution.OrderId = stOrderID;
        //                    _execution.ExecType = "0";
        //                    _execution.MajorVersionNo = ++majorVersion;
        //                    _execution.MinorVersionNo = 0;
        //                    _execution.ExecBroker = newOrder.TradingSessionCode;
        //                    _execution.SymbolSfx = newOrder.SymbolSuffix;
        //                    _execution.SideId = getSideKeyByValue(newOrder.Side);
        //                    _execution.LastShares = 0;
        //                    _execution.ClientOrderId = newOrder.FirmOrderID;//"2019020517525B";
        //                    _execution.ExecNature = "1";
        //                    _execution.ClientId = stClientID;
        //                    _execution.Symbol = newOrder.Symbol;
        //                    _execution.OrderStatus = "0";
        //                    _execution.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
        //                    _execution.TradeDate = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd");
        //                    _execution.OrderQty = int.Parse(newOrder.Quantity);
        //                    _execution.LeavesQty = int.Parse(newOrder.Quantity);
        //                    //double stopPrice = double.TryParse(newOrder.StopPrice, out stopPrice) ? stopPrice : 0;
        //                    _execution.Price = double.Parse(newOrder.LimitPrice);
        //                    _execution.TimeInForceId= _dTimeInForce[newOrder.TimeInForce];
        //                    _execution.OrderType = "2";
        //                    _execution.MessageTag = 8;
        //                    _execution.OrignatingUserId = stTrader; 
        //                    _execution.Rule80A = "A";
        //                    _execution.OrderMarker = 1;
        //                    _execution.OrdRouteStatus = 0;
        //                    _execution.TargetLocationId = stBoothID;
        //                    _execution.ExecRefId = "0";

        //                    #endregion tableExecution

        //                    _order.OrderDetails.Add(_orderDetail);
        //                    _order.Executions.Add(_execution);
        //                    if (newOrder.srLsttransactions != null)
        //                    {
        //                        for (int i = 1; i <= newOrder.srLsttransactions.Values.Count; i++)
        //                        {
        //                            ITran transaction = newOrder.srLsttransactions[i];
        //                            if (transaction is EX)
        //                            {
        //                                Execution execution = new Execution();
        //                                EX ex = transaction as EX;
        //                                execution.BoothId = stBoothID;
        //                                execution.OrderId = stOrderID;
        //                                execution.ExecType = "2";
        //                                //execution.MajorVersionNo = i;
        //                                execution.MajorVersionNo = _execution.MajorVersionNo + i;

        //                                // Mendatory Fields
        //                                execution.ExecId = (++inExecId).ToString();
        //                                execution.MinorVersionNo = 0;
        //                                execution.LastShares = ex.Quantity;
        //                                execution.Price = ex.Price;
        //                                execution.Rule80A = ex.CapacityCode;
        //                                execution.OrderStatus = "2";
        //                                execution.ClientOrderId = newOrder.FirmOrderID;//"2019020517525B";
        //                                execution.ClientId = stClientID;
        //                                execution.TransactTime = Helper.ConvertPlainStringToDatetime(newOrder.OrderReceivedTimestamp, "yyyy-MM-dd HH:mm:ss");
        //                                execution.TimeInForceId = _dTimeInForce[newOrder.TimeInForce];
        //                                execution.SideId = getSideKeyByValue(newOrder.Side);
        //                                execution.OrderType = "2";
        //                                execution.Rule80A = "A";
        //                                execution.OrignatingUserId = stTrader;
        //                                execution.OrderMarker = 1;
        //                                execution.OrdRouteStatus = 0;
        //                                execution.TargetLocationId = stBoothID;
        //                                execution.ExecRefId = "0";
        //                                execution.ExecNature = "1";
        //                                execution.Symbol = newOrder.Symbol;
        //                                _order.Executions.Add(execution);
        //                            }
        //                            else if (transaction is RT)
        //                            {
        //                                Console.WriteLine("Fill tables for RT Details");
        //                            }
        //                        }
        //                    }
        //                    transactionalDBContext.tb_Order.Add(_order);
        //                }
        //            }
        //            transactionalDBContext.SaveChanges();
        //        }
    }
}
