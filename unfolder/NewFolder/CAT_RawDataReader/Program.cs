﻿using CAT_RawDataReader.FileFormatSpecifier;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace CAT_RawDataReader
{
    class Program
    {
        static void Main(string[] args)
        {

            #region ExpandoObject    
            //var xDoc = XDocument.Load(new StreamReader("Config\\Object.xml"));

            //List<string> listNodes = new List<string>() {"EX"};
            //dynamic root = new ExpandoObject();

            //Helper_Classes.ExpandoObjectHelper.Parse(root, xDoc.Root, listNodes);

            //Console.WriteLine(root);

            ///*Helper_Classes.XmlToDynamicClass.Parse(root, xDoc.Elements().First());

            //Console.WriteLine(root.contacts.contact.Count);
            //Console.WriteLine(root.contacts.contact[0].firstName);
            //Console.WriteLine(root.contacts.contact[0].id);
            //*/
            //Console.ReadLine();

            #endregion ExpandoObject

            //IA - TODO: Get Path of Config XML from main App.cofing
            string stXMLPath = "Config/Config.xml";
            string stSpecsPath = "Config/Properties.xml";
            Console.WriteLine("File Data Utility Starts!!!");
            Console.WriteLine("Read Specifications from XML!!!");

            SpecificationManager specs = new SpecificationManager(stSpecsPath); 
            Console.WriteLine("Loading File Configurations from XML!!!");
            ConfigManager.FileManager fileManager = new ConfigManager.FileManager();
            ConfigManager.File[] files = fileManager.DeserializeXMLConfig(stXMLPath);
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].IsEnabled)
                {
                    Console.WriteLine($"Loading File: {files[i].FileName} with BoothId: {files[i].BoothId}!.");
                    TransactionCacheFile fileCache = new TransactionCacheFile(files[i]);
                    fileCache.LoadTransactionalData();
                }
            }         
             Console.ReadLine();
        }
    }
}
