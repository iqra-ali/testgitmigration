﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CAT_RawDataReader
{
    public class Reader1
    {
        private TransactionCacheFile transactionsCacheFile;
        public TransactionCacheFile TransactionCacheFile { get => transactionsCacheFile; private set => transactionsCacheFile = value; }


        public Reader1()
        {
            //TransactionCacheFile = new TransactionCacheFile();
        }

        public void LoadData(ConfigManager.File File)
        {
            if (TransactionCacheFile != null)
            {
                TransactionCacheFile.LoadTransactionalData(/*File*/);
            }

        }

       
    }
}
