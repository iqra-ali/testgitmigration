﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAT_RawDataReader.Helper_Classes
{
    public abstract class ITran
    {
         string RecordType { get; set; }
         string FirmorderID { get; set; }
         string Symbol { get; set; }
    }
}
