﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAT_RawDataReader.Helper_Classes
{
    public class RT: ITran
    {
        public string RecordType { get; set; }
        public string FirmorderID { get; set; }
        public string RouteTimestamp { get; set; }
        public string SentToFirmMPID { get; set; }
        public string RoutedOrderID { get; set; }
        public string Symbol { get; set; }
        protected decimal RouteQuantity { get; set; }
        public string RouteMethodCode { get; set; }
        public string RouteTypeIndicator { get; set; }
        protected double RoutePrice { get; set; }
        public string FutureUse_1 { get; set; }
        public string ROEMemo { get; set; }
        public decimal Quantity { get => RouteQuantity; set => RouteQuantity = value; }
        public double Price { get => RoutePrice; set => RoutePrice = value; }

        public RT()
        {

        }
        public RT(string[] fields)
        {
            int _index = 0;
            RecordType = fields[_index];
            FirmorderID = fields[++_index];
            RouteTimestamp = fields[++_index];
            SentToFirmMPID = fields[++_index];
            RoutedOrderID = fields[++_index];
            Symbol = fields[++_index];
            Quantity = decimal.Parse(fields[++_index]);
            RouteMethodCode = fields[++_index];
            RouteTypeIndicator = fields[++_index];
            Price = double.Parse(fields[++_index]);
            FutureUse_1 = fields[++_index];
            ROEMemo = fields[++_index];
        }
    }
}
