﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAT_RawDataReader.Helper_Classes
{
    public class NW
    {
        public string RecordType { get; set; }
        public string FirmOrderID { get; set; }
        public string RoutingFirmMPID { get; set; }
        public string RoutedOrderID { get; set; }
        public string OrderReceivedTimestamp { get; set; }
        public string Symbol { get; set; }
        public string SymbolSuffix { get; set; }
        public string Side { get; set; }
        public string Quantity { get; set; }
        public string LimitPrice { get; set; }
        public string FutureUse_1 { get; set; }
        public string FutureUse_2 { get; set; }
        public string StopPrice { get; set; }
        public string TimeInForce { get; set; }
        public string ExpirationDate { get; set; }
        public string ExpirationTime { get; set; }
        public string FutureUse_3 { get; set; }
        public string FutureUse_4 { get; set; }
        public string TradingSessionCode { get; set; }
        public string FutureUse_5 { get; set; }
        public string FutureUse_6 { get; set; }
        public string OrderOriginationCode { get; set; }
        public string AccountTypeCode { get; set; }
        public string MemberTypeCode { get; set; }
        public string FutureUse_7 { get; set; }
        public string FutureUse_8 { get; set; }
        public string ROEMemo { get; set; }

        public int MajorVersion = 0;

        public readonly SortedList<int, ITran> srLsttransactions = null;
        //private ITran _transaction;
        //public ITran Transaction { get => _transaction; private set => _transaction = value; }


        public NW(string [] fields)
        {

            MajorVersion = 1;
            int _index = 0;
            RecordType = fields[_index];
            FirmOrderID = fields[++_index];
            RoutingFirmMPID = fields[++_index];
            RoutedOrderID = fields[++_index];
            OrderReceivedTimestamp = fields[++_index];
            Symbol = fields[++_index];
            SymbolSuffix = fields[++_index];
            Side = fields[++_index];
            Quantity = fields[++_index];
            LimitPrice = fields[++_index];
            FutureUse_1 = fields[++_index];
            FutureUse_2 = fields[++_index];
            StopPrice = fields[++_index];
            TimeInForce = fields[++_index];
            ExpirationDate = fields[++_index];
            ExpirationTime = fields[++_index];
            FutureUse_3 = fields[++_index];
            FutureUse_4 = fields[++_index];
            TradingSessionCode = fields[++_index];
            FutureUse_5 = fields[++_index];
            FutureUse_6 = fields[++_index];
            OrderOriginationCode = fields[++_index];
            AccountTypeCode = fields[++_index];
            MemberTypeCode = fields[++_index];
            FutureUse_7 = fields[++_index];
            FutureUse_8 = fields[++_index];
            ROEMemo = fields[++_index];
            //lsTransaction = new List<ITransaction>();
            //lsEX = new List<EX>();
            srLsttransactions  = new SortedList<int, ITran>();
            //Transaction = new ITran();
        }

        public void FillListRT(string[] fields)
            {
            RT rt = new RT(fields);
            this.srLsttransactions.Add(MajorVersion++, rt);
            }

        public void FillListEX(string[] fields)
        {
            EX ex = new EX(fields);
            this.srLsttransactions.Add(MajorVersion++, ex);

            Console.WriteLine("");

        }
    }
} 


