﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UserVersion
    {
        public string BoothId { get; set; }
        public string UserId { get; set; }
        public int Version { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
