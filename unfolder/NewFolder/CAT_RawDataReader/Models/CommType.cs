﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class CommType
    {
        public string CommissionType { get; set; }
        public string CommissionTypeDesc { get; set; }
    }
}
