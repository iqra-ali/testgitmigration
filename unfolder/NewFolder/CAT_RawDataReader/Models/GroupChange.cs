﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class GroupChange
    {
        public GroupChange()
        {
            GroupOrderDetails = new HashSet<GroupOrderDetails>();
        }

        public string GroupId { get; set; }
        public int MinorVersionNo { get; set; }
        public int MajorVersionNo { get; set; }
        public decimal CumQty { get; set; }
        public decimal LeavesQty { get; set; }
        public string OrignatingUserId { get; set; }
        public int MessageTag { get; set; }
        public string TimeInForceId { get; set; }
        public DateTime? TransactTime { get; set; }
        public double? ClosestToMarketPrice { get; set; }
        public string OriginatingUserDesc { get; set; }

        public virtual Groups Group { get; set; }
        public virtual ICollection<GroupOrderDetails> GroupOrderDetails { get; set; }
    }
}
