﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Task
    {
        public int? TaskType { get; set; }
        public int Version { get; set; }
        public string Instruction { get; set; }
        public string OrderId { get; set; }
        public string BoothId { get; set; }
        public DateTime TransactTime { get; set; }
        public string SubOrderId { get; set; }
        public string OriginUser { get; set; }
        public string TransactTimeMicroseconds { get; set; }
    }
}
