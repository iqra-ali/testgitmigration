﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AdminUsers
    {
        public string UserId { get; set; }
        public string Pwd { get; set; }
        public string FirstName { get; set; }
        public string UserDesignation { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public int? RoleId { get; set; }
        public string BoothGroupId { get; set; }

        public virtual AdminBoothGroupTable BoothGroup { get; set; }
        public virtual AdminRoles Role { get; set; }
    }
}
