﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AlfieldSequence
    {
        public int DisplaySeqNumId { get; set; }
        public int BoothDestAlgoId { get; set; }
        public int DisplayFieldId { get; set; }
        public int DisplaySeqNum { get; set; }
    }
}
