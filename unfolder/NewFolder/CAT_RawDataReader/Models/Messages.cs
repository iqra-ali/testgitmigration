﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Messages
    {
        public int MessageTag { get; set; }
        public string MessageTagDesc { get; set; }
    }
}
