﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class ExecType
    {
        public string ExecType1 { get; set; }
        public string ExecTypeDesc { get; set; }
    }
}
