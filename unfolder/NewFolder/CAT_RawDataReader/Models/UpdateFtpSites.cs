﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UpdateFtpSites
    {
        public UpdateFtpSites()
        {
            UpdateMomsVersion = new HashSet<UpdateMomsVersion>();
        }

        public int FtpId { get; set; }
        public string FtpName { get; set; }
        public int? FtpPort { get; set; }
        public string FtpUserName { get; set; }
        public string FtpPwd { get; set; }
        public string FtpIpaddress { get; set; }

        public virtual ICollection<UpdateMomsVersion> UpdateMomsVersion { get; set; }
    }
}
