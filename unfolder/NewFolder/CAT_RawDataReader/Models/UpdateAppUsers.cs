﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UpdateAppUsers
    {
        public int AppUpdateUserId { get; set; }
        public string UserId { get; set; }
        public string BoothId { get; set; }
        public int ApplicationId { get; set; }
        public bool? Active { get; set; }
    }
}
