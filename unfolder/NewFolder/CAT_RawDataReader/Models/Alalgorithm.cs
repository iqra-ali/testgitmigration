﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Alalgorithm
    {
        public int AlgorithmId { get; set; }
        public string AlgorithmName { get; set; }
        public bool? IsIso { get; set; }
        public bool? IsPair { get; set; }
    }
}
