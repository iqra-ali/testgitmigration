﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OtinstructionInfo
    {
        public string OtinstructionId { get; set; }
        public string OtinstructionName { get; set; }
    }
}
