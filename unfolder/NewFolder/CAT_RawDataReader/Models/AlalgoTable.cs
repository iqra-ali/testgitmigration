﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AlalgoTable
    {
        public int AlgoId { get; set; }
        public string AlgoName { get; set; }
        public string AlgoFixTag { get; set; }
    }
}
