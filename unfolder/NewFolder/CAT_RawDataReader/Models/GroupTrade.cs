﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class GroupTrade
    {
        public GroupTrade()
        {
            GroupAllocation = new HashSet<GroupAllocation>();
        }

        public string AllocGroupId { get; set; }
        public string BoothId { get; set; }
        public int? CumTradeQty { get; set; }
        public int? CumQty { get; set; }
        public double? AvgPrice { get; set; }
        public string ClientId { get; set; }
        public string Side { get; set; }
        public string Symbol { get; set; }
        public string SymbolSfx { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? TransactTime { get; set; }
        public string AllocAccount { get; set; }
        public string SenderSubId { get; set; }
        public string TargetSubId { get; set; }
        public string SendingComputerId { get; set; }
        public string TargetComputerId { get; set; }
        public string OnBehalfOfComputerId { get; set; }
        public string DeliverToComputerId { get; set; }
        public string SenderLocationId { get; set; }
        public string TargetLocationId { get; set; }
        public string OrignatingUserId { get; set; }
        public DateTime? SendingTime { get; set; }
        public DateTime? OrigSendingTime { get; set; }
        public string SecExchange { get; set; }
        public string Currency { get; set; }

        public virtual ICollection<GroupAllocation> GroupAllocation { get; set; }
    }
}
