﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class BoothClientsDetail
    {
        public string ClientId { get; set; }
        public string BoothId { get; set; }
    }
}
