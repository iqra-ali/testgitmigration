﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UserGroups
    {
        public string UserId { get; set; }
        public string FirmId { get; set; }
        public string GroupId { get; set; }
    }
}
