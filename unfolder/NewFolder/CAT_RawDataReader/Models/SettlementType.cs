﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class SettlementType
    {
        public string SettlementId { get; set; }
        public string SettlementDesc { get; set; }
    }
}
