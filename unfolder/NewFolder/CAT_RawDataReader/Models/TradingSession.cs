﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class TradingSession
    {
        public string TradingSessionCode { get; set; }
        public string RangeFrom { get; set; }
        public string RangeTo { get; set; }
    }
}
