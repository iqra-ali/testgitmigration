﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UnsolFillsReference
    {
        public string ExecId { get; set; }
        public string UnsolRefId { get; set; }
        public string BoothId { get; set; }
        public DateTime ExecTimeStamp { get; set; }
    }
}
