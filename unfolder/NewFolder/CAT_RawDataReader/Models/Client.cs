﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Client
    {
        public Client()
        {
            Broker = new HashSet<Broker>();
            Trader = new HashSet<Trader>();
            UserCdetail = new HashSet<UserCdetail>();
        }

        public string ClientId { get; set; }
        public string CompanyName { get; set; }
        public string ContactPerson { get; set; }
        public string Telephone { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ClientDesc { get; set; }
        public double? Commission { get; set; }

        public virtual ICollection<Broker> Broker { get; set; }
        public virtual ICollection<Trader> Trader { get; set; }
        public virtual ICollection<UserCdetail> UserCdetail { get; set; }
    }
}
