﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class SettlCurrency
    {
        public string SettlCurrencyId { get; set; }
        public string SettlCurrencyDesc { get; set; }
    }
}
