﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OtdisplayHeadingField
    {
        public string OtdisplayHeadingFieldId { get; set; }
        public string OtdisplayHeadingFieldName { get; set; }
        public string OtcheckBoxHeading { get; set; }
    }
}
