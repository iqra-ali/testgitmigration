﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class ExecTransType
    {
        public string ExecTransType1 { get; set; }
        public string ExecTransTypeDesc { get; set; }
    }
}
