﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Currency
    {
        public string CurrencyId { get; set; }
        public string CurrencyDesc { get; set; }
    }
}
