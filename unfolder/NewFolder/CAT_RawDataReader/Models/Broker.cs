﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Broker
    {
        public string BrokerId { get; set; }
        public string BrokerDesc { get; set; }
        public double? Commission { get; set; }
        public string Email { get; set; }
        public string ClientId { get; set; }

        public virtual Client Client { get; set; }
    }
}
