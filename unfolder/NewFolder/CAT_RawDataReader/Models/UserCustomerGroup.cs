﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class UserCustomerGroup
    {
        public string BoothId { get; set; }
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public int ActionType { get; set; }
    }
}
