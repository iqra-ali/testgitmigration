﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class CustomerGroups
    {
        public string BoothId { get; set; }
        public string ClientId { get; set; }
        public string GroupId { get; set; }
    }
}
