﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Nmssymbols
    {
        public string Symbol { get; set; }
        public string SymbolName { get; set; }
    }
}
