﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class TimeInForce
    {
        public string TimeInForceId { get; set; }
        public string TimeInForceDesc { get; set; }
    }
}
