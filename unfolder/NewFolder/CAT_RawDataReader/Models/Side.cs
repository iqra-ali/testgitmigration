﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Side
    {
        public string SideId { get; set; }
        public string SideDesc { get; set; }
    }
}
