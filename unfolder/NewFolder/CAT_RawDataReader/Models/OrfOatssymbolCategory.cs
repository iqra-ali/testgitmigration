﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OrfOatssymbolCategory
    {
        public string Category { get; set; }
        public string MarketCenterId { get; set; }
    }
}
