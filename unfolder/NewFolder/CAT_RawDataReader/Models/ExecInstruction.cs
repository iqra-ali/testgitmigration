﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class ExecInstruction
    {
        public string ExecInst { get; set; }
        public string ExecInstructionDesc { get; set; }
    }
}
