﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Rule80A
    {
        public string Rule80A1 { get; set; }
        public string Rule80Adesc { get; set; }
    }
}
