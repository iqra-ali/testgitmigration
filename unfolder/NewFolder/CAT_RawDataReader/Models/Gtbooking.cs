﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Gtbooking
    {
        public string GtbookingInst { get; set; }
        public string GtbookingDesc { get; set; }
    }
}
