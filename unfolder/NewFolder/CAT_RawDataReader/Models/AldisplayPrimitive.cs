﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AldisplayPrimitive
    {
        public int DisplayPrimitiveId { get; set; }
        public string DisplayPrimitiveName { get; set; }
    }
}
