﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Orders
    {
        public Orders()
        {
            Executions = new HashSet<Execution>();
            OrderDetails = new HashSet<OrderDetails>();
        }

        public string BoothId { get; set; }
        public string OrderId { get; set; }
        public string SecurityType { get; set; }
        public string ClientId { get; set; }
        public string Symbol { get; set; }
        public string Account { get; set; }
        public DateTime? FutSettDate { get; set; }
        public string SymbolSfx { get; set; }
        public string SecurityExchange { get; set; }
        public double? PrevClosePx { get; set; }
        public double? Commision { get; set; }
        public string Text { get; set; }
        public string ExecBroker { get; set; }
        public string ExDestination { get; set; }
        public string BrokerDesc { get; set; }
        public string SettlementId { get; set; }
        public string Rule80A { get; set; }
        public string ProcessCode { get; set; }
        public string CommissionType { get; set; }
        public string OrderCapacity2 { get; set; }
        public DateTime? WriteInTime { get; set; }
        public string TargetSubId { get; set; }
        public string BoothOrdIdent { get; set; }
        public string SenderLocationId { get; set; }
        public string TargetLocationId { get; set; }
        public string SenderSubId { get; set; }
        public string ExternalOrderId { get; set; }
        public string LocationId { get; set; }
        public string ContactName { get; set; }
        public string Oatsinst { get; set; }
        public string SecurityInfo { get; set; }

        public virtual Booth Booth { get; set; }
        public virtual ICollection<Execution> Executions { get; set; }
        public virtual ICollection<OrderDetails> OrderDetails { get; set; }
    }
}
