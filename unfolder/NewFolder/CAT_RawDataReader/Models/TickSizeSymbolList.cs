﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class TickSizeSymbolList
    {
        public string TickerSymbol { get; set; }
        public string SecurityName { get; set; }
        public string ListingExchange { get; set; }
        public string EffectiveDate { get; set; }
        public string TickSizePilotGroup { get; set; }
    }
}
