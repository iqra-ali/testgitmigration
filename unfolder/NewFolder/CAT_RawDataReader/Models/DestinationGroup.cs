﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class DestinationGroup
    {
        public string BoothId { get; set; }
        public string DestinationGroupId { get; set; }
        public string Destination { get; set; }
    }
}
