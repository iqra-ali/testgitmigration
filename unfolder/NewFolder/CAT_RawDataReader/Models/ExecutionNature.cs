﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class ExecutionNature
    {
        public string ExecNature { get; set; }
        public string ExecNatureDesc { get; set; }
    }
}
