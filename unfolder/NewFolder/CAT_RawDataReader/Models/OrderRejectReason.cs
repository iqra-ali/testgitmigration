﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OrderRejectReason
    {
        public string OrdRejReason { get; set; }
        public string OrdRejDesc { get; set; }
    }
}
