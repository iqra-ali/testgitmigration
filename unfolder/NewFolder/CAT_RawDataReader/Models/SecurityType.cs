﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class SecurityType
    {
        public string SecurityType1 { get; set; }
        public string SecurityTypeDesc { get; set; }
    }
}
