﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Destination
    {
        public string DestinationId { get; set; }
        public string DestinationDesc { get; set; }
    }
}
