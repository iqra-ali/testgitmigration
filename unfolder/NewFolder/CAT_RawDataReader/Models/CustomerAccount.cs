﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class CustomerAccount
    {
        public string BoothId { get; set; }
        public string ClientId { get; set; }
        public string MasterAccount { get; set; }
        public double? Commission { get; set; }
        public string Cmta { get; set; }
        public string RepCode { get; set; }
        public bool? IsOasys { get; set; }
        public string ClientType { get; set; }
        public bool? IsFacilitationAccount { get; set; }
        public string Tif { get; set; }
        public string Instr { get; set; }
        public string Stlmt { get; set; }
        public string AccountType { get; set; }
        public string Cvrd { get; set; }
        public string Range { get; set; }
        public double? Markup { get; set; }
        public bool? IsAutoHoldFills { get; set; }
        public string GiveUp { get; set; }
        public string CommissionType { get; set; }
        public double? OptionsCommission { get; set; }
        public string OptionsCommissionType { get; set; }
        public string Destination { get; set; }
        public string Ltid { get; set; }
        public string OptionDestination { get; set; }
        public string ComplexDestination { get; set; }
        public bool? IsSme { get; set; }
        public string Fdid { get; set; }
        public string Catsendertype { get; set; }
        public string Catacctype { get; set; }
        public string Catdepttype { get; set; }
        public string Catimid { get; set; }
    }
}
