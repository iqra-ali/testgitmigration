﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AdminRoles
    {
        public AdminRoles()
        {
            AdminUsers = new HashSet<AdminUsers>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDesc { get; set; }
        public int RightsGroupId { get; set; }

        public virtual AdminRightsGroup RightsGroup { get; set; }
        public virtual ICollection<AdminUsers> AdminUsers { get; set; }
    }
}
