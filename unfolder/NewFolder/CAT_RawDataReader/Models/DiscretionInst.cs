﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class DiscretionInst
    {
        public string DiscretionInst1 { get; set; }
        public string DiscretionInstDesc { get; set; }
        public double? DiscretionOffset { get; set; }
    }
}
