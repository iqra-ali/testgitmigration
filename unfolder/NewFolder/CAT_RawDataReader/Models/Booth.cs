﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Booth
    {
        public Booth()
        {
            BoothUsers = new HashSet<BoothUsers>();
            Orders = new HashSet<Orders>();
        }

        public string BoothId { get; set; }
        public string FirmId { get; set; }
        public string BoothDescription { get; set; }
        public string Nyseroom { get; set; }
        public string BoothLocation { get; set; }
        public bool? VolumeReport { get; set; }
        public bool? AuditReport { get; set; }

        public virtual Firms Firm { get; set; }
        public virtual ICollection<BoothUsers> BoothUsers { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
