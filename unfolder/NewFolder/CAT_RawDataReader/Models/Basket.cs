﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Basket
    {
        public string BoothId { get; set; }
        public int BasketId { get; set; }
        public string BasketName { get; set; }
        public int BasketMajorVersion { get; set; }
        public int BasketMinorVersion { get; set; }
        public string BasketTransType { get; set; }
        public DateTime? TrasactTime { get; set; }
        public string OriginatingUser { get; set; }
        public string ExtraFields { get; set; }
    }
}
