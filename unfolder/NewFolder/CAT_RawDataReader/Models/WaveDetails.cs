﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class WaveDetails
    {
        public string BoothId { get; set; }
        public int BasketId { get; set; }
        public int WaveId { get; set; }
        public int WaveMajorVersion { get; set; }
        public string OrderId { get; set; }
    }
}
