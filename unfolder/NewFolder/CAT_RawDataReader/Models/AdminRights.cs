﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class AdminRights
    {
        public int RightsId { get; set; }
        public string RightsName { get; set; }
        public string RightsDesc { get; set; }
    }
}
