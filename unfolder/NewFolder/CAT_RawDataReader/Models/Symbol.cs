﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Symbol
    {
        public string Symbol1 { get; set; }
        public string Company { get; set; }
        public int? Post { get; set; }
        public string Panel { get; set; }
    }
}
