﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OptAttribute
    {
        public string OptAttribute1 { get; set; }
        public string OptAttributeDesc { get; set; }
    }
}
