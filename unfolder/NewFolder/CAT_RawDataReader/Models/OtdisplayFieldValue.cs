﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OtdisplayFieldValue
    {
        public string OtdisplayFieldValueId { get; set; }
        public string OtdisplayFieldValueName { get; set; }
        public string OtdataType { get; set; }
    }
}
