﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class OrderType
    {
        public string OrderType1 { get; set; }
        public string OrderTypeDesc { get; set; }
    }
}
