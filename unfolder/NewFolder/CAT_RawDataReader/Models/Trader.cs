﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class Trader
    {
        public string TraderId { get; set; }
        public string TraderDesc { get; set; }
        public string ClientId { get; set; }
        public double? Commission { get; set; }
        public string Email { get; set; }

        public virtual Client Client { get; set; }
    }
}
