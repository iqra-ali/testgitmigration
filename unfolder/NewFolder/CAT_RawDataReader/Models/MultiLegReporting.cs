﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class MultiLegReporting
    {
        public string MultiLegReportingType { get; set; }
        public string MultiLegReportingDesc { get; set; }
    }
}
