﻿using System;
using System.Collections.Generic;

namespace CAT_RawDataReader.Models
{
    public partial class ExecRestatement
    {
        public string ExecRestatementReason { get; set; }
        public string ExecRestatementDesc { get; set; }
    }
}
