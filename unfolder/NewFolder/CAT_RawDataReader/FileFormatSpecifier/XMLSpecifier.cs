﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CAT_RawDataReader.FileFormatSpecifier
{
    [Serializable]
    public class EventField
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Type { get; set; }
        [XmlAttribute]
        public int MappingIndex { get; set; }
    }

    [Serializable()]
    public class Event
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }
        [XmlElement("Field")]
        public List<EventField> Fields { get; set; }
    }

    [Serializable()]
    [XmlRoot("Specification")]
    public class Events
    {
        [XmlArray("Events")]
        [XmlArrayItem("Event", typeof(Event))]
        public List<Event> EventInfos { get; set; }
    }
}
