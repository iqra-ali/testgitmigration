﻿using CAT_RawDataReader.Helper_Classes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TypeGenerator;

namespace CAT_RawDataReader.FileFormatSpecifier
{
    public class SpecificationManager
    {

        private Events eventInfo;
        public object newOrder;
        public List<object> executions;  
        public SpecificationManager(string stSpecificationPath)
        {
            DeserializeEvents(stSpecificationPath);
            GenerateEvents();
        }

        private void DeserializeEvents(string stPath)
        {

            if (string.IsNullOrEmpty(stPath))
            {
                Console.WriteLine("Specification Config file path should not be null or empty.");
                return;
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Events));
                using (XmlReader reader = XmlReader.Create(stPath))
                {
                    eventInfo = serializer.Deserialize(reader) as Events;
                }
            }
            catch (InvalidOperationException exp)
            {
                Console.WriteLine("Error occured in loading specification configuration. File: {0}.", stPath);
                Exception innerException = exp;
                if (exp.InnerException != null)
                {
                    innerException = exp.InnerException;
                    Console.WriteLine("Error occured in loading specification configuration. See inner Exception for details.");
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine("Error occured in loading specification configuration. File: {0}.", stPath);
                throw exp;
                
            }
        }
        private void GenerateEvents()
        {
            try
            {
                ParentEvent parentObject = new ParentEvent();
                parentObject.srLsttransactions.Add(1, new RT());
                int _MajorVersion = 0;
                executions = new List<object>();
                foreach (var _event in eventInfo.EventInfos)
                {
                    List<Field> fields = new List<Field>();
                    
                    foreach (var f in _event.Fields)
                    {
                        Type dataType = (f.Type).GetType();
                        fields.Add(new Field(f.Name, dataType));
                    }
                    if (_event.Name.Equals("NW"))
                    {
                        //TODO
                        //ParentEvent parentObject = new ParentEvent();
                        parentObject = (ParentEvent)DynamicTypeGenerator.CreateDynamicObject<ParentEvent>(fields); // create ref here and hold it to parentevent
                        //custom lambda expressions  
                       
                        //var props = newOrder.GetType().GetProperties(); // propertie fields
                        //var mems = newOrder.GetType().GetMember("srLsttransactions")[0];  //
                    }
                    else
                    {
                        ITran obj = DynamicTypeGenerator.CreateDynamicObject<ITran>(fields) as ITran; // EX.properties  
                    
                        if (parentObject != null && obj != null)
                        {
                            if (parentObject.srLsttransactions == null)
                            {
                                parentObject.srLsttransactions = new SortedList<int, ITran>();
                            }
                            parentObject.srLsttransactions.Add(_MajorVersion++, obj);
                            
                        }
                        
                        // inherit these executions with ITran
                    }
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
           
        }

        private void SetObjectProperty(string propertyName, string value, object obj)
        {


            PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);
            // make sure object has the property we are after
            if (propertyInfo != null)
            {
                //get the list property value 
                //cast it to a required list
                //now append the value to this list // add 
                //set the value  
                propertyInfo.SetValue(obj, value, null);
            }
        }
    }
}
