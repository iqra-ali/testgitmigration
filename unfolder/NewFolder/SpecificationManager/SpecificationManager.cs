﻿using Specification.DataStructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BusinessObjects;
using System.Xml;
using BusinessObjects.Specification;
using GlobalUtils;
using BusinessObjects.Exceptions;

namespace Specification
{
    public class SpecificationManager
    {
        private SpecificationInfo specificationInfo;
        private GlobalUtils.RuleEngineConsumer ruleEngineConsumer;
        private Dictionary<string, object> setupContext;
        private TypeManager typeManager = new TypeManager();

        public void LoadCATSpecs(CATFileConfig config)
        {
            AppLogger.LogInformation(string.Format("Reading Specification configuration from {0}", config.CATSpecsXML));
            ReadSpecification(config.CATSpecsXML);
            AppLogger.LogInformation("Reading Specification configuration completed...");

            AppLogger.LogInformation(string.Format("Reading Specification Rules and Condition from {0}", config.CATSpecsXML));
            ruleEngineConsumer = new GlobalUtils.RuleEngineConsumer(config.CATSpecsXML);
            AppLogger.LogInformation("Reading Specification Rules and Condition completed...");

            setupContext = new Dictionary<string, object>();

            AppLogger.LogInformation("Executing Startup Rules.");
            foreach (var ruleRef in specificationInfo.SetupContext.RulesRef)
                if (!string.IsNullOrEmpty(ruleRef.Key))
                    ruleEngineConsumer.ExecuteRules(ruleRef.Key, setupContext);
            AppLogger.LogInformation("Startup Rules Executed.");
        }

        private void ReadSpecification(string specsFilePath)
        {
            if (string.IsNullOrEmpty(specsFilePath))
                throw new ConfigException(specsFilePath, "Specification Config file path should not be null or empty.");

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SpecificationInfo));
                using (XmlReader reader = XmlReader.Create(specsFilePath))
                {
                    specificationInfo = serializer.Deserialize(reader) as SpecificationInfo;
                }
            }
            catch (InvalidOperationException exp)
            {
                AppLogger.LogError(string.Format("Error occured in loading specification configuration. File: {0}.", specsFilePath));

                Exception innerException = exp;
                if (exp.InnerException != null)
                    innerException = exp.InnerException;

                throw new ConfigException(specsFilePath, "Error occured in loading specification configuration. See inner Exception for details.", innerException);
            }
            catch (Exception exp)
            {
                AppLogger.LogError(string.Format("Error occured in loading specification configuration. File: {0}.", specsFilePath));
                throw exp;
            }

            try
            {
                typeManager.GenerateType(specificationInfo);
            }
            catch(Exception exp)
            {
                AppLogger.LogError("Error Generating custom types for CAT Specification. Please refer to types of fields and specialized type. See inner exception for details.");
                throw new CATSpecificationException("Error Generating custom types for CAT Specification. Please refer to types of fields and specialized type. See inner exception for details.", exp);
            }
        }


        public void GenerateCATEvents(BusinessObjects.TransactionsCache trasactionsCache, BusinessObjects.StaticDataCache staticDataCache)
        {
            Dictionary<string, object> dataContext = new Dictionary<string, object>(setupContext)
            {
                { "StaticData", staticDataCache },
                { "TransactionalData", trasactionsCache }
            };
            AppLogger.LogDebug("Generating CAT Events...");
            GenerateCATEvents(trasactionsCache.OrderTransactions.Values, dataContext);
            AppLogger.LogDebug("CAT Event Generation Completed...");
        }

        private void GenerateCATEvents(IEnumerable<OrderLifeCycle> orderLifeCycles, Dictionary<string, object> dataContext)
        {
            if (orderLifeCycles == null || !orderLifeCycles.Any())
            {
                AppLogger.LogWarn("Transactional Cache does not contains any order.");
                return;
            }

            foreach (OrderLifeCycle orderLifeCycle in orderLifeCycles)
            {
                AppLogger.LogDebug(string.Format("Generating CAT Events for BoothID: {0} OrderID: {1}", orderLifeCycle.Order.BoothId, orderLifeCycle.Order.OrderId));
                GenerateCATEvents(orderLifeCycle, dataContext);
                AppLogger.LogDebug(string.Format("CAT Event Completed for BoothID: {0} OrderID: {1}", orderLifeCycle.Order.BoothId, orderLifeCycle.Order.OrderId));
            }
        }

        private void GenerateCATEvents(OrderLifeCycle orderLifeCycle, Dictionary<string, object> dataContext)
        {
            Dictionary<string, object> innerDataContext = new Dictionary<string, object>(dataContext)
            {
                { "OLC", orderLifeCycle }
            };

            if (orderLifeCycle.Transactions == null || !orderLifeCycle.Transactions.Any())
            {
                // Should never reach here.
                AppLogger.LogWarn("Order does not contains any transaction.");
                return;
            }

            foreach (var transaction in orderLifeCycle.Transactions)
            {
                AppLogger.LogDebug(string.Format("Generating CAT Events for transaction Type: {0}, Version: {1}", transaction.Value.Type, transaction.Value.VersionSeq));
                CATEvent catEvent = GenerateCATEvents(transaction.Value, innerDataContext);
                AppLogger.LogDebug(string.Format("CAT Event Completed for transaction Type: {0}, Version: {1}", transaction.Value.Type, transaction.Value.VersionSeq));

                if (catEvent != null)
                {
                    AppLogger.LogDebug("Inserting generated event in cache.");
                    orderLifeCycle.CATEvents.Add(transaction.Key, catEvent);
                    AppLogger.LogDebug("Generated event is inserted in cache.");
                }
                else
                {
                    AppLogger.LogDebug(string.Format("Transaction is not CAT reportable Type: {0}, Version: {1}", transaction.Value.Type, transaction.Value.VersionSeq));
                }
            }
        }

        private CATEvent GenerateCATEvents(ITransaction transaction, Dictionary<string, object> dataContext)
        {
            Dictionary<string, object> innerDataContext = new Dictionary<string, object>(dataContext)
            {
                { "CurrentTransaction", transaction }
            };

            foreach (var eventInfo in specificationInfo.EventInfos)
            {
                try
                {
                    if (!EventIsReportable(eventInfo, innerDataContext))
                    {
                        AppLogger.LogDebug(string.Format("Event {0} is NOT reportable for this transaction.", eventInfo.Name));
                        continue;
                    }
                }
                catch (Exception exp)
                {
                    AppLogger.LogError(string.Format("Error evaluating condition having key {0} for the event {1}.", eventInfo.ConditionKey, eventInfo.Name));
                    throw new CATEventSpecificationException(eventInfo.Name, string.Format("Error evaluating condition having key {0} for the event. See inner exception for details.", eventInfo.ConditionKey), exp);
                }

                AppLogger.LogDebug(string.Format("Creating {0} event instance.", eventInfo.Name));
                CATEvent catEvent = EventInstantiator.GenerateEvent(eventInfo, specificationInfo, typeManager);
                AppLogger.LogDebug(string.Format("{0} event instance created.", eventInfo.Name));

                AppLogger.LogDebug(string.Format("Start populating fields of Event {0}", eventInfo.Name));
                PopulateFields(eventInfo, ref catEvent, innerDataContext);
                AppLogger.LogDebug(string.Format("End populating fields of Event {0}", eventInfo.Name));

                if (!string.IsNullOrEmpty(eventInfo.RuleKey))
                {
                    try
                    {
                        AppLogger.LogDebug(string.Format("Applying Rule {0} on {1} event", eventInfo.RuleKey, eventInfo.Name));
                        ruleEngineConsumer.ExecuteRules(eventInfo.RuleKey, innerDataContext);
                        AppLogger.LogDebug(string.Format("Rule {0} applied on {1} event", eventInfo.RuleKey, eventInfo.Name));
                    }
                    catch (Exception exp)
                    {
                        AppLogger.LogError(string.Format("Error Applying Rule having key {0} for the event {1}.", eventInfo.RuleKey, eventInfo.Name));
                        throw new CATEventSpecificationException(eventInfo.Name, string.Format("Error Applying Rule having key {0} for the event. See inner exception for details.", eventInfo.RuleKey), exp);
                    }
                }

                return catEvent;
            }
            return null;
        }
        private void PopulateFields(EventInfo eventInfo, ref CATEvent catEvent, Dictionary<string, object> dataContext)
        {
            Dictionary<string, object> innerDataContext = new Dictionary<string, object>(dataContext)
            {
                { "CurrentEvent", catEvent }
            };

            foreach (var eventFieldInfo in eventInfo.Fields)
            {
                try
                {
                    if (!EventFieldIsReportable(eventFieldInfo, innerDataContext))
                    {
                        AppLogger.LogDebug(string.Format("Field {0} is NOT reportable for this event.", eventFieldInfo.Name));
                        continue;
                    }
                }
                catch (Exception exp)
                {
                    AppLogger.LogError(string.Format("Error evaluating condition having key {0} for the Event: {1} and Field {2}", eventFieldInfo.ConditionKey, eventInfo.Name, eventFieldInfo.Name));
                    throw new CATEventSpecificationException(eventInfo.Name, eventFieldInfo.Name,
                        string.Format("Error evaluating condition having key {0} for the field. See inner exception for details.", eventFieldInfo.ConditionKey),
                        exp);
                }

                ICATField catField = catEvent[eventFieldInfo.Name];

                AppLogger.LogDebug(string.Format("Populating {0} field.", eventFieldInfo.Name));
                PopulateField(eventFieldInfo, ref catField, innerDataContext);
                AppLogger.LogDebug(string.Format("Field {0} is populated.", eventFieldInfo.Name));


                AppLogger.LogDebug(string.Format("Updating Field {0} on Event.", eventFieldInfo.Name));
                catEvent = innerDataContext["CurrentEvent"] as CATEvent;

                if (catEvent != null)
                    catEvent[eventFieldInfo.Name] = catField;

                AppLogger.LogDebug(string.Format("Field {0} updated.", eventFieldInfo.Name));
            }
        }

        private void PopulateField(EventFieldInfo eventFieldInfo, ref ICATField catField, Dictionary<string, object> dataContext)
        {
            Dictionary<string, object> innerDataContext = new Dictionary<string, object>(dataContext)
            {
                { "CurrentField", catField }
            };

            if (eventFieldInfo.IsHardCoded && !string.IsNullOrEmpty(eventFieldInfo.Value))
            {
                AppLogger.LogDebug(string.Format("Applying Hard coded value to Field {0}", eventFieldInfo.Name));
                catField.Value = eventFieldInfo.Value;
                AppLogger.LogDebug(string.Format("Hard coded value Applied to Field {0}", eventFieldInfo.Name));
            }
            else if (!string.IsNullOrEmpty(eventFieldInfo.EvaluateValue))
            {
                try
                {
                    AppLogger.LogDebug(string.Format("Evaluating value expression for Field {0}", eventFieldInfo.Name));
                    catField.Value = System.Linq.Dynamic.Core.DynamicExpressionParser.ParseLambda(null, eventFieldInfo.EvaluateValue, innerDataContext).Compile().DynamicInvoke();
                    AppLogger.LogDebug(string.Format("Value expression evaluated and assigned to Field {0}", eventFieldInfo.Name));
                }
                catch (Exception exp)
                {
                    AppLogger.LogError(string.Format("Error evaluating value having key {0} for the Event: {1} and Field {2}", eventFieldInfo.EvaluateValue, (dataContext["CurrentEvent"] as CATEvent).EventType, eventFieldInfo.Name));
                    throw new CATEventSpecificationException((dataContext["CurrentEvent"] as CATEvent).EventType, eventFieldInfo.Name,
                        string.Format("Error evaluating value having key {0} for the field. See inner exception for details.", eventFieldInfo.EvaluateValue)
                        , exp);
                }
            }

            if (!string.IsNullOrEmpty(eventFieldInfo.RuleKey))
            {
                try
                {
                    AppLogger.LogDebug(string.Format("Applying Rule {0} on {1} field", eventFieldInfo.RuleKey, eventFieldInfo.Name));
                    ruleEngineConsumer.ExecuteRules(eventFieldInfo.RuleKey, innerDataContext);
                    AppLogger.LogDebug(string.Format("Rule {0} applied on {1} field", eventFieldInfo.RuleKey, eventFieldInfo.Name));
                }
                catch (Exception exp)
                {
                    AppLogger.LogError(string.Format("Error Applying Rule having key {0} for the Event: {1} and Field {2}", eventFieldInfo.RuleKey, (dataContext["CurrentEvent"] as CATEvent).EventType, eventFieldInfo.Name));
                    throw new CATEventSpecificationException((dataContext["CurrentEvent"] as CATEvent).EventType, eventFieldInfo.Name,
                        string.Format("Error Applying Rule having key {0} for the field. See inner exception for details.", eventFieldInfo.RuleKey)
                        , exp);
                }
            }

            // Update in case reference is changed
            catField = innerDataContext["CurrentField"] as ICATField;

            if (catField != null && catField.Value == null && !string.IsNullOrEmpty(eventFieldInfo.DefaultValue))
            {
                AppLogger.LogDebug(string.Format("Applying Default value to Field {0}", eventFieldInfo.Name));
                catField.Value = eventFieldInfo.DefaultValue;
                AppLogger.LogDebug(string.Format("Default value Applied to Field {0}", eventFieldInfo.Name));
            }
        }

        private bool EventIsReportable(EventInfo eventInfo, Dictionary<string, object> dataContext)
        {
            if (eventInfo.Disabled)
                return false;

            if (!string.IsNullOrEmpty(eventInfo.ConditionKey) && !ruleEngineConsumer.EvaluateCondition(eventInfo.ConditionKey, dataContext))
                return false;

            return true;
        }

        private bool EventFieldIsReportable(EventFieldInfo eventFieldInfo, Dictionary<string, object> dataContext)
        {
            if (eventFieldInfo.Disabled)
                return false;

            if (!string.IsNullOrEmpty(eventFieldInfo.ConditionKey) && !ruleEngineConsumer.EvaluateCondition(eventFieldInfo.ConditionKey, dataContext))
                return false;

            return true;
        }
    }
}
