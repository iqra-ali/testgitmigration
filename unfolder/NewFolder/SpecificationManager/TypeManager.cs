﻿using Specification;
using Specification.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core.CustomTypeProviders;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Specification
{
    public class TypeManager
    {
        private Dictionary<string, Type> typeNames = new Dictionary<string, Type>();
        private SpecializedTypeGenerator specializedTypeGenerator = new SpecializedTypeGenerator();

        public void GenerateType(Events specificationInfo)
        {
            //foreach (var specializedType in specificationInfo.FieldSpecializedTypes)
            //{
            //    GenerateSpecializedType(specificationInfo, specializedType);
            //}

            foreach(var fieldInfo in specificationInfo.EventInfos)
            {
                GenerateFieldType(specificationInfo, fieldInfo.Fields);
            }

            //foreach (var eventInfo in specificationInfo.EventInfos)
            //{
            //    GenerateEventType(specificationInfo, eventInfo);
            //}
        }

        private void GenerateEventType(SpecificationInfo specificationInfo, EventInfo eventInfo)
        {
            IEnumerable<DynamicPropertyInfo> propInfo = GetPropInfo(specificationInfo, eventInfo.Fields.Select(x => x.Name));
            Type type = specializedTypeGenerator.CreateType(eventInfo.Name, propInfo, false, typeof(BusinessObjects.Specification.CATEvent), eventInfo.Name);
            typeNames.Add(eventInfo.Name, type);
        }

        public Type GetType(FieldInfo field)
        {
            Type type;
            if (!string.IsNullOrEmpty(field.SpecializedType))
                type = GetType(field.SpecializedType);
            else if (!string.IsNullOrEmpty(field.TypeArgs) || field.SubFields.Any())
                type = GetType(GenerateTypeNameForNonSpecialized(GetType(field.DataType), field.TypeArgs, field.SubFields.Select(x => new DynamicPropertyInfo(x.Name, GetType(x)))));
            else
                type = GetType(field.DataType);

            return type;
        }

        public Type GetType(string typeFullName)
        {
            if (!typeNames.ContainsKey(typeFullName))
            {
                string namespaceRegex = @"(?<Namespace>([A-Za-z][A-Za-z0-9]+\.+)*)";
                string typeNameRegex = @"(?<TypeName>([A-Za-z][A-Za-z0-9]*)(`(?<SubTypeCount>[0-9]+))*)";
                string subType = @"(\[((((?<SubTypes>(((?'Open'\[)[^\[\]]*)+((?'Close-Open'\])[^\[\]]*?)+)*?)*?(?(Open)(?!)))|((?<SubTypes>[^\[\],]+)*)),*\s*)*\])?";
                //string subType = @"(\[((?<SubTypes>(((?<Open>\[)[^\[\]]*)+((?<Close-Open>\])[^\[\]]*)+)*?([^\[\],])*(,[^\[\]])*)*?(?(Open)(?!)))\])*";
                string assemblyRegex = @"(,\s*(?<Assembly>[A-Za-z0-9]+))*";
                Regex reg = new Regex(string.Concat(namespaceRegex, typeNameRegex, subType, assemblyRegex));
                var m = reg.Match(typeFullName);

                string assembly = m.Groups["Assembly"].Value;
                string namespaces = m.Groups["Namespace"].Value;
                string typeName = m.Groups["TypeName"].Value;
                string subTypeCount = m.Groups["SubTypeCount"].Value;

                List<Type> types = new List<Type>();
                if (typeFullName.EndsWith("?"))
                    typeFullName = string.Format("System.Nullable`1[{0}]", typeFullName.TrimEnd('?'));


                if (string.IsNullOrEmpty(assembly))
                {
                    Type type = Type.GetType(typeFullName, false, true);
                    if (type != null)
                        return type;

                    if (string.IsNullOrEmpty(namespaces))
                    {
                        assembly = "BusinessObjects";
                        namespaces = "BusinessObjects.Specification.";
                    }
                }

                if(m.Groups["SubTypes"].Captures.Count > 0)
                {
                    if (!string.IsNullOrEmpty(subTypeCount) && (int.Parse(subTypeCount) != m.Groups["SubTypes"].Captures.Count))
                        throw new InvalidOperationException(string.Format("Invalid Type {0}", typeFullName));

                    if (string.IsNullOrEmpty(subTypeCount))
                        typeName = string.Format("{0}`{1}", typeName, m.Groups["SubTypes"].Captures.Count);

                    foreach (var subtype in m.Groups["SubTypes"].Captures)
                        types.Add(GetType(subtype.ToString().Trim('[',']')));

                    Type baseType = Type.GetType(string.Format("{0}{1}, {2}", namespaces, typeName, assembly));
                    return baseType?.MakeGenericType(types.ToArray());
                }

                return Type.GetType(string.Format("{0}{1}, {2}", namespaces, typeName, assembly));

            }

            return typeNames[typeFullName];
        }

        private void GenerateSpecializedType(SpecificationInfo specificationInfo, SpecializedType specializedType)
        {
            Type genericType = GetType(specializedType.GenericType);
            object[] args = ParameterParser.ParseParameters(specializedType.TypeArgs).ToArray();

            if (genericType != null && args.Length == 0 && !specializedType.FieldProperties.Any())
            {
                typeNames.Add(specializedType.Name, genericType);
                return;
            }

            foreach (var fieldName in specializedType.FieldProperties)
            {
                var field = specificationInfo.FieldInfos[fieldName];
                GenerateFieldType(specificationInfo, field);
            }

            IEnumerable<DynamicPropertyInfo> propInfo = GetPropInfo(specificationInfo, specializedType.FieldProperties);

            Type type = specializedTypeGenerator.CreateType(specializedType.Name, propInfo, true, genericType, args);

            typeNames.Add(specializedType.Name, type);
            string nonSpecializedName = GenerateTypeNameForNonSpecialized(genericType, specializedType.TypeArgs, propInfo);
            if (!typeNames.ContainsKey(nonSpecializedName))
                typeNames.Add(nonSpecializedName, type);
        }

        private void GenerateFieldType(Events specificationInfo, EventField fieldInfo)
        {
            if (!string.IsNullOrEmpty(fieldInfo.DataType))
            {
                Type genericType = GetType(fieldInfo.DataType);
                if (genericType == null)
                    throw new TypeLoadException(string.Format("Cannot find a type names {0}", fieldInfo.DataType));

                //object[] args = ParameterParser.ParseParameters(fieldInfo.TypeArgs).ToArray();

                if (genericType != null)
                {
                    string typeName = GenerateTypeNameForNonSpecialized(genericType, null, null);
                    if (!typeNames.ContainsKey(typeName))
                    {
                        Type type = specializedTypeGenerator.CreateType(typeName, null, true, genericType, null);
                        typeNames.Add(typeName, type);
                    }
                }
            }
        }

        private string GenerateTypeNameForNonSpecialized(Type genericType, string args, IEnumerable<DynamicPropertyInfo> propInfos)
        {
            StringBuilder typeName = new StringBuilder(genericType.Name);
            if (genericType.IsGenericType)
                typeName.AppendFormat("<{0}>", string.Join(",", genericType.GenericTypeArguments.Select(x => x.Name)));

            if (!string.IsNullOrEmpty(args))
                typeName.AppendFormat("({0})", args);

            if(propInfos != null && propInfos.Any())
                typeName.AppendFormat("{{{0}}}", string.Join(",", propInfos));

            return typeName.ToString();
        }

        private IEnumerable<DynamicPropertyInfo> GetPropInfo(Event specificationInfo, IEnumerable<string> fieldNames)
        {
            foreach (var fieldName in fieldNames)
            {
                var field = specificationInfo.EventInfos[fieldName];
                //yield return new DynamicPropertyInfo(field.Name, (!string.IsNullOrEmpty(field.DataType)) ? GetType(field.DataType) : GetType(field.SpecializedType));
                yield return new DynamicPropertyInfo(field.Name, GetType(field));
            }
        }
    }
}
