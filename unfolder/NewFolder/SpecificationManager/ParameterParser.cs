﻿using GlobalUtils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Specification
{
    internal class ParameterParser
    {
        private static object parseAsDecimal(string text, bool isHexadecimal, string qualifierString)
        {
            char qualifier = (!string.IsNullOrEmpty(qualifierString)) ? qualifierString[0] : '\0';
            if (qualifier == 'F' || qualifier == 'f')
            {
                float f;
                if (float.TryParse(text, NumberStyles.Float, CultureInfo.InvariantCulture, out f))
                    return f;
            }

            if (qualifier == 'M' || qualifier == 'm')
            {
                decimal de;
                if (decimal.TryParse(text, NumberStyles.Number, CultureInfo.InvariantCulture, out de))
                    return de;
            }

            // Try double conversion even if try parse fails for float and decimal
            double d;
            if (double.TryParse(text, NumberStyles.Number, CultureInfo.InvariantCulture, out d))
                return d;

            return null;
        }
        private static object parseAsInteger(string text, bool isHexadecimal, string qualifier)
        {
            if (text[0] != '-')
            {
                if (isHexadecimal)
                {
                    text = text.Substring(2);
                }
                ulong value;
                if (!ulong.TryParse(text, isHexadecimal ? NumberStyles.HexNumber : NumberStyles.Integer, CultureInfo.CurrentCulture, out value))
                    return null;

                if (!string.IsNullOrEmpty(qualifier))
                {
                    if (qualifier == "U" || qualifier == "u") return (uint)value;
                    if (qualifier == "L" || qualifier == "l") return (long)value;

                    // in case of UL, just return
                    return value;
                }

                if (value <= int.MaxValue) return (int)value;
                if (value <= uint.MaxValue) return (uint)value;
                if (value <= long.MaxValue) return (long)value;

                return value;
            }
            else
            {
                long value;
                if (!long.TryParse(text, isHexadecimal ? NumberStyles.HexNumber : NumberStyles.Integer, CultureInfo.CurrentCulture, out value))
                    return null;

                if (!string.IsNullOrEmpty(qualifier))
                {
                    if (qualifier == "L" || qualifier == "l") return value;
                }

                if (value <= int.MaxValue) return (int)value;

                return value;
            }
        }
        private static object parseAsNumericValue(string arg)
        {
            string text = arg;
            bool isHexadecimal = text.StartsWith(text[0] == '-' ? "-0x" : "0x", StringComparison.OrdinalIgnoreCase);
            char[] qualifierLetters = isHexadecimal
                                          ? new[] { 'U', 'u', 'L', 'l' }
                                          : new[] { 'U', 'u', 'L', 'l', 'F', 'f', 'D', 'd', 'M', 'm' };
            char[] decimalQualifierLetters = { 'F', 'f', 'D', 'd', 'M', 'm' };
            char last = text[text.Length - 1];
            string qualifier = null;

            if (qualifierLetters.Contains(last))
            {
                int pos = text.Length - 1, count = 0;
                while (qualifierLetters.Contains(text[pos]))
                {
                    ++count;
                    --pos;
                }
                qualifier = text.Substring(text.Length - count, count);
                text = text.Substring(0, text.Length - count);
            }
            if (!text.Contains(".") && !decimalQualifierLetters.Contains(last))
                return parseAsInteger(text, isHexadecimal, qualifier);
            else
                return parseAsDecimal(text, isHexadecimal, qualifier);
        }
        public static string[] SplitParameters(string input)
        {
            if (string.IsNullOrEmpty(input))
                return new string[0];
            //Regex csvSplit = new Regex("(?:^|,)([\"\\{\\(](?:[^\"\\}\\)])*[\"\\}\\)]|[^,]*)", RegexOptions.Compiled);
            string regexStr = @"^((((?'test'([^\(\)\{\}\[\],]+(?'Open'\()[^\(\)]*)+((?'Close-Open'\)))+)(?(Open)(?!)))|((?'test'(([^\(\)\{\}\[\],]+(?'Open1'\[)[^\[\]]*)+((?'Close1-Open1'\]))+)(?(Open1)(?!)))((((?'Open2'\{)[^\{\}]*)+((?'Close2-Open2'\}))+)(?(Open2)(?!)))*)|((?'test'((?'Open3'\{)[^\{\}]*)+((?'Close3-Open3'\}))+)(?(Open3)(?!)))|((?'test'[^\(\)\{\}\[\],]+)))(,|$))+";
            Regex csvSplit = new Regex(regexStr, RegexOptions.Compiled);
            var match = csvSplit.Match(input);

            if (match == null)
                throw new InvalidOperationException("No Parameters");
            return match.Groups["test"].Captures.Cast<Capture>().Select(x => x.ToString()).ToArray();
            //remaining = input.Substring(match.Index + match.Length).TrimStart(' ', ',');

            //return match.Value.TrimStart(' ', ',').Trim(' ');
        }
        private static object parseParameter(string arg)
        {
            if (arg[0] == '{')
            {
                var subArgs = ParseParameters(arg.Trim('{', '}'));
                var uniqueTypes = new HashSet<Type>(subArgs.Select(x => x.GetType()).ToArray());
                if (uniqueTypes.Count == 1)
                {
                    MethodInfo castMethod = typeof(Enumerable).GetMethod("Cast").MakeGenericMethod(uniqueTypes.First());
                    MethodInfo arrayMethod = typeof(Enumerable).GetMethod("ToArray").MakeGenericMethod(uniqueTypes.First());
                    return arrayMethod.Invoke(null, new object[] { castMethod.Invoke(null, new object[] { subArgs }) });
                }
                else
                    return subArgs;
            }
            if (arg[0] == '"')
            {
                return arg.Trim('"');
            }
            else if (arg[0] == '\'')
            {
                string str = arg.Trim('\'');
                if (str.Length == 1)
                    return str[0];
                else
                    return str;
            }
            else if ((arg[0] == '-' && char.IsDigit(arg[1])) || char.IsDigit(arg[0]))
            {
                return parseAsNumericValue(arg) ?? arg;
            }
            else
                return arg;
        }
        public static IEnumerable<object> ParseParameters(string input)
        {
            string[] args = SplitParameters(input);
            foreach (var arg in args)
            {
                yield return parseParameter(arg);
            }
            yield break;
        }
    }
}
