﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using GlobalUtils;
using Specification.DataStructure;
using System.Collections.ObjectModel;
using BusinessObjects.Specification;

namespace Specification
{
    public class DynamicPropertyInfo
    {
        public DynamicPropertyInfo(string propertyName, Type propertyType)
        {
            PropertyName = propertyName;
            PropertyType = propertyType;
        }
        public string PropertyName { get; private set; }
        public Type PropertyType { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}:{1}", PropertyName, PropertyType.FullName);
        }
    }

    internal class SpecializedTypeGenerator
    {
        private readonly AssemblyName assemblyName;
        private readonly AssemblyBuilder assemblyBuilder;
        private readonly ModuleBuilder moduleBuilder;
        private readonly Hashtable typeHash;

        private readonly CustomAttributeBuilder RuleEngineCustomTypeAttributeBuilder = new CustomAttributeBuilder(typeof(RuleEngine.Attributes.RuleEngineCustomTypeAttribute).GetConstructor(Type.EmptyTypes), new object[0]);

        //private readonly MethodInfo EqualityComparerDefault = EqualityComparer.GetMethod("get_Default", BindingFlags.Static | BindingFlags.Public, null, Type.EmptyTypes, null);
        //private readonly MethodInfo EqualityComparerEquals = EqualityComparer.GetMethod("Equals", BindingFlags.Instance | BindingFlags.Public, null, new[] { EqualityComparerGenericArgument, EqualityComparerGenericArgument }, null);
        //private readonly MethodInfo EqualityComparerGetHashCode = EqualityComparer.GetMethod("GetHashCode", BindingFlags.Instance | BindingFlags.Public, null, new[] { EqualityComparerGenericArgument }, null);


        public SpecializedTypeGenerator()
        {
            assemblyName = new AssemblyName("MyDynamicAssembly");
            assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name);

            typeHash = new Hashtable
            {
                [typeof(sbyte)] = OpCodes.Ldind_I1,
                [typeof(byte)] = OpCodes.Ldind_U1,
                [typeof(char)] = OpCodes.Ldind_U2,
                [typeof(short)] = OpCodes.Ldind_I2,
                [typeof(ushort)] = OpCodes.Ldind_U2,
                [typeof(int)] = OpCodes.Ldind_I4,
                [typeof(uint)] = OpCodes.Ldind_U4,
                [typeof(long)] = OpCodes.Ldind_I8,
                [typeof(ulong)] = OpCodes.Ldind_I8,
                [typeof(bool)] = OpCodes.Ldind_I1,
                [typeof(double)] = OpCodes.Ldind_R8,
                [typeof(float)] = OpCodes.Ldind_R4
            };

        }


        public Type CreateType(string typeName, IEnumerable<DynamicPropertyInfo> propInfo, bool createParameterCtor = true, Type baseType = null, params object[] baseArgsConstValue)
        {

            Type type = moduleBuilder.GetType(typeName.Replace(",","\\,"));
            if (type != null)
                return type;

            TypeBuilder typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.AnsiClass | TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.AutoLayout | TypeAttributes.BeforeFieldInit, baseType);
            typeBuilder.SetCustomAttribute(RuleEngineCustomTypeAttributeBuilder);
            // Equals
            MethodBuilder equals = typeBuilder.DefineMethod("Equals", MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig, CallingConventions.HasThis, typeof(bool), new[] { typeof(object) });
            equals.DefineParameter(1, ParameterAttributes.In, "value");
            
            ILGenerator ilgeneratorEquals = equals.GetILGenerator();
            ilgeneratorEquals.DeclareLocal(typeBuilder.AsType());
            ilgeneratorEquals.Emit(OpCodes.Ldarg_1);
            ilgeneratorEquals.Emit(OpCodes.Isinst, typeBuilder.AsType());
            ilgeneratorEquals.Emit(OpCodes.Stloc_0);
            ilgeneratorEquals.Emit(OpCodes.Ldloc_0);

            Label equalsLabel = ilgeneratorEquals.DefineLabel();

            // GetHashCode()
            MethodBuilder getHashCode = typeBuilder.DefineMethod("GetHashCode", MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig, CallingConventions.HasThis, typeof(int), Type.EmptyTypes);
            ILGenerator ilgeneratorGetHashCode = getHashCode.GetILGenerator();
            ilgeneratorGetHashCode.DeclareLocal(typeof(int));

            MethodBuilder methodEquality = typeBuilder.DefineMethod("op_Equality", MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, typeof(bool), new Type[] { typeBuilder, typeBuilder });
            ILGenerator ilgeneratorEquality = methodEquality.GetILGenerator();
            ilgeneratorEquality.Emit(OpCodes.Ldarg_0);
            ilgeneratorEquality.Emit(OpCodes.Ldarg_1);
            ilgeneratorEquality.Emit(OpCodes.Callvirt, equals);
            ilgeneratorEquality.Emit(OpCodes.Ret);

            MethodBuilder methodInEquality = typeBuilder.DefineMethod("op_Inequality", MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, typeof(bool), new Type[] { typeBuilder, typeBuilder });
            ILGenerator ilgeneratorInEquality = methodInEquality.GetILGenerator();
            ilgeneratorInEquality.Emit(OpCodes.Ldarg_0);
            ilgeneratorInEquality.Emit(OpCodes.Ldarg_1);
            ilgeneratorInEquality.Emit(OpCodes.Call, methodEquality);
            ilgeneratorInEquality.Emit(OpCodes.Ldc_I4_0);
            ilgeneratorInEquality.Emit(OpCodes.Ceq);
            ilgeneratorInEquality.Emit(OpCodes.Ret);

            if (baseType != null)
            {
                Type[] baseArgsType = new Type[baseArgsConstValue.Length];

                for (int i = 0; i < baseArgsConstValue.Length; i++)
                {
                    baseArgsType[i] = baseArgsConstValue[i].GetType();
                }

                var baseConstructor = baseType.GetConstructor(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance, null, baseArgsType, null);
                if (baseConstructor != null)
                {
                    // Create a parameterless (default) constructor.
                    var constructor = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, null);

                    var ilGenerator = constructor.GetILGenerator();

                    // Generate constructor code
                    ilGenerator.Emit(OpCodes.Ldarg_0);                // push &quot;this&quot; onto stack.

                    foreach (object arg in baseArgsConstValue)
                    {
                        EmitArgs(ilGenerator, arg);
                    }

                    ilGenerator.Emit(OpCodes.Call, baseConstructor);  // call base constructor

                    ilGenerator.Emit(OpCodes.Nop);                    // C# compiler add 2 NOPS, so
                    ilGenerator.Emit(OpCodes.Nop);                    // we'll add them, too.

                    ilGenerator.Emit(OpCodes.Ret);                    // Return
                }

                var baseConversionConstructor = baseType.GetConstructor(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance, null, new List<Type>(baseArgsType) { typeof(object) }.ToArray(), null);
                if (baseConversionConstructor != null)
                {
                    Type[] convertTypes = Helper.AvailableCastType(baseType, baseType);

                    // Create a parameterless (default) constructor.
                    var conversionConstructor = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[] { typeof(object) });

                    var conversionIlGenerator = conversionConstructor.GetILGenerator();

                    // Generate constructor code
                    conversionIlGenerator.Emit(OpCodes.Ldarg_0);                // push &quot;this&quot; onto stack.

                    foreach (object arg in baseArgsConstValue)
                    {
                        EmitArgs(conversionIlGenerator, arg);
                        //GCHandle handle = GCHandle.Alloc(arg, GCHandleType.Weak);
                        //IntPtr pointer1 = GCHandle.ToIntPtr(handle);
                        //var local = conversionIlGenerator.DeclareLocal(arg.GetType());
                        //conversionIlGenerator.Emit(OpCodes.Ldloca, local);
                        //conversionIlGenerator.Emit(OpCodes.Ldc_I8, pointer1.ToInt64());
                        //conversionIlGenerator.Emit(OpCodes.Cpobj, arg.GetType());
                        //conversionIlGenerator.Emit(OpCodes.Ldloc, local);

                        //if (arg.GetType().IsValueType)
                        //{
                        //    conversionIlGenerator.Emit(OpCodes.Unbox, arg.GetType());
                        //    EmitValueType(arg, conversionIlGenerator);
                        //}
                    }

                    conversionIlGenerator.Emit(OpCodes.Ldarg_1);

                    conversionIlGenerator.Emit(OpCodes.Call, baseConversionConstructor);  // call base constructor

                    conversionIlGenerator.Emit(OpCodes.Nop);                    // C# compiler add 2 NOPS, so
                    conversionIlGenerator.Emit(OpCodes.Nop);                    // we'll add them, too.

                    conversionIlGenerator.Emit(OpCodes.Ret);                    // Return

                    foreach (Type convertType in convertTypes)
                    {
                        var methodBuilder = typeBuilder.DefineMethod("op_Implicit", MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, typeBuilder, new Type[] { convertType });

                        var methodIlGenerator = methodBuilder.GetILGenerator();

                        methodIlGenerator.Emit(OpCodes.Ldarg_0);
                        if (convertType.IsValueType)
                        {
                            methodIlGenerator.Emit(OpCodes.Box, convertType);
                        }
                        methodIlGenerator.DeclareLocal(typeBuilder);
                        methodIlGenerator.Emit(OpCodes.Newobj, conversionConstructor);
                        methodIlGenerator.Emit(OpCodes.Stloc_0);
                        methodIlGenerator.Emit(OpCodes.Ldloc_0);
                        methodIlGenerator.Emit(OpCodes.Ret);
                    }
                }

                Type[] reverseConvertTypes = Helper.AvailableCastTypeFromParameter(baseType, baseType);
                foreach (Type convertType in reverseConvertTypes)
                {
                    bool isImplicit = true;
                    var baseConversionMethod = baseType.GetMethod("op_Implicit", new Type[] { baseType });
                    if (baseConversionMethod == null)
                    {
                        baseConversionMethod = baseType.GetMethod("op_Explicit", new Type[] { baseType });
                        isImplicit = false;
                    }

                    if (baseConversionMethod == null)
                        continue;

                    var methodBuilder = typeBuilder.DefineMethod(isImplicit ? "op_Implicit" : "op_Explicit", MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, convertType, new Type[] { typeBuilder });

                    var methodIlGenerator = methodBuilder.GetILGenerator();

                    methodIlGenerator.Emit(OpCodes.Ldarg_0);

                    methodIlGenerator.Emit(OpCodes.Call, baseConversionMethod);
                    methodIlGenerator.Emit(OpCodes.Ret);
                }

                var baseEquals = baseType.GetMethod("Equals", BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance , null, new Type[] { typeof(object) }, null);

                ilgeneratorEquals.Emit(OpCodes.Brfalse, equalsLabel);
                ilgeneratorEquals.Emit(OpCodes.Ldarg_0);
                ilgeneratorEquals.Emit(OpCodes.Ldarg_1);
                ilgeneratorEquals.Emit(OpCodes.Call, baseEquals);

                var baseGetHashCode = baseType.GetMethod("GetHashCode", BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance, null, Type.EmptyTypes, null);

                ilgeneratorGetHashCode.Emit(OpCodes.Ldarg_0);
                ilgeneratorGetHashCode.Emit(OpCodes.Call, baseGetHashCode);
                ilgeneratorGetHashCode.Emit(OpCodes.Stloc_1);
            }

            if (propInfo != null)
            {
                Type[] types = propInfo.Select(p => p.PropertyType).ToArray();
                string[] names = propInfo.Select(p => p.PropertyName).ToArray();

                var fields = new FieldBuilder[names.Length];

                // There are two for cycles because we want to have all the getter methods before all the other methods
                for (int i = 0; i < names.Length; i++)
                {
                    // field
                    fields[i] = typeBuilder.DefineField($"<{names[i]}>i__Field", types[i], FieldAttributes.Private | FieldAttributes.InitOnly);

                    PropertyBuilder property = typeBuilder.DefineProperty(names[i], PropertyAttributes.None, CallingConventions.HasThis, types[i], Type.EmptyTypes);
                    CustomAttributeBuilder serializationOrderAttributeBuilder = new CustomAttributeBuilder(typeof(SerializationOrderAttribute).GetConstructor(new Type[] { typeof(int) }), new object[] { i });
                    property.SetCustomAttribute(serializationOrderAttributeBuilder);

                    // getter
                    MethodBuilder getter = typeBuilder.DefineMethod($"get_{names[i]}", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName, CallingConventions.HasThis, types[i], null);
                    ILGenerator ilgeneratorGetter = getter.GetILGenerator();
                    ilgeneratorGetter.Emit(OpCodes.Ldarg_0);
                    ilgeneratorGetter.Emit(OpCodes.Ldfld, fields[i]);
                    ilgeneratorGetter.Emit(OpCodes.Ret);
                    property.SetGetMethod(getter);

                    // setter
                    MethodBuilder setter = typeBuilder.DefineMethod($"set_{names[i]}", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName, CallingConventions.HasThis, null, new[] { types[i] });

                    // workaround for https://github.com/dotnet/corefx/issues/7792
                    setter.DefineParameter(1, ParameterAttributes.In, names[i]);

                    ILGenerator ilgeneratorSetter = setter.GetILGenerator();

                    if (typeof(ICATField).IsAssignableFrom(types[i]))
                    {
                        //Label elseLabel = ilgeneratorSetter.DefineLabel();
                        //Label endLabel = ilgeneratorSetter.DefineLabel();
                        //ilgeneratorSetter.Emit(OpCodes.Ldarg_1);
                        //ilgeneratorSetter.Emit(OpCodes.Ldnull);
                        //MethodInfo equality = types[i].GetMethod("op_Equality", BindingFlags.Public | BindingFlags.Static, null, new Type[] { types[i], types[i] }, null);
                        //ilgeneratorSetter.Emit(OpCodes.Call, equality);

                        //ilgeneratorSetter.Emit(OpCodes.Brfalse_S, elseLabel);
                        //ilgeneratorSetter.Emit(OpCodes.Ldarg_0);

                        //ilgeneratorSetter.Emit(OpCodes.Ldfld, fields[i]);

                        //PropertyInfo valuePropertyInfo = types[i].GetProperty("Value");
                        //if (Nullable.GetUnderlyingType(valuePropertyInfo.PropertyType) == null)
                        //{
                        //    ilgeneratorSetter.Emit(OpCodes.Ldnull);
                        //}
                        //else
                        //{
                        //    var local = ilgeneratorSetter.DeclareLocal(valuePropertyInfo.PropertyType);
                        //    ilgeneratorSetter.Emit(OpCodes.Ldloca_S, local);
                        //    ilgeneratorSetter.Emit(OpCodes.Initobj, valuePropertyInfo.PropertyType);
                        //    ilgeneratorSetter.Emit(OpCodes.Ldloc, local);
                        //}
                        //MethodInfo valueSetter = valuePropertyInfo.SetMethod;
                        //ilgeneratorSetter.Emit(OpCodes.Callvirt, valueSetter);

                        //ilgeneratorSetter.Emit(OpCodes.Br_S, endLabel);
                        //ilgeneratorSetter.MarkLabel(elseLabel);
                        ilgeneratorSetter.Emit(OpCodes.Ldarg_0);
                        ilgeneratorSetter.Emit(OpCodes.Ldarg_1);
                        ilgeneratorSetter.Emit(OpCodes.Stfld, fields[i]);
                        //ilgeneratorSetter.MarkLabel(endLabel);
                        ilgeneratorSetter.Emit(OpCodes.Ldarg_0);
                        ilgeneratorSetter.Emit(OpCodes.Ldfld, fields[i]);
                        ilgeneratorSetter.Emit(OpCodes.Ldstr, names[i]);
                        var nameSetter = types[i].GetProperty("Name").SetMethod;
                        ilgeneratorSetter.Emit(OpCodes.Callvirt, nameSetter);
                    }
                    else
                    {
                        ilgeneratorSetter.Emit(OpCodes.Ldarg_0);
                        ilgeneratorSetter.Emit(OpCodes.Ldarg_1);
                        ilgeneratorSetter.Emit(OpCodes.Stfld, fields[i]);
                    }

                    ilgeneratorSetter.Emit(OpCodes.Ret);
                    property.SetSetMethod(setter);
                }

                if (names.Length == 0)
                {
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldc_I4_0);
                }
                else
                {
                    // As done by Roslyn
                    // Note that initHash can vary, because string.GetHashCode() isn't "stable" for different compilation of the code
                    int initHash = 0;

                    for (int i = 0; i < names.Length; i++)
                    {
                        initHash = unchecked(initHash * (-1521134295) + fields[i].Name.GetHashCode());
                    }

                    // Note that the CSC seems to generate a different seed for every anonymous class
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldc_I4, initHash);
                }

                for (int i = 0; i < names.Length; i++)
                {
                    Type EqualityComparer = typeof(EqualityComparer<>).MakeGenericType(types[i]);
                    Type EqualityComparerGenericArgument = EqualityComparer.GetGenericArguments()[0];
                    MethodInfo EqualityComparerDefault = EqualityComparer.GetMethod("get_Default", BindingFlags.Static | BindingFlags.Public);
                    MethodInfo EqualityComparerEquals = EqualityComparer.GetMethod("Equals", new[] { EqualityComparerGenericArgument, EqualityComparerGenericArgument });
                    MethodInfo EqualityComparerGetHashCode = EqualityComparer.GetMethod("GetHashCode", new[] { EqualityComparerGenericArgument });

                    // Illegal one-byte branch at position: 9. Requested branch was: 143.
                    // So replace OpCodes.Brfalse_S to OpCodes.Brfalse
                    ilgeneratorEquals.Emit(OpCodes.Brfalse, equalsLabel);
                    ilgeneratorEquals.Emit(OpCodes.Call, EqualityComparerDefault);
                    ilgeneratorEquals.Emit(OpCodes.Ldarg_0);
                    ilgeneratorEquals.Emit(OpCodes.Ldfld, fields[i]);
                    ilgeneratorEquals.Emit(OpCodes.Ldloc_0);
                    ilgeneratorEquals.Emit(OpCodes.Ldfld, fields[i]);
                    ilgeneratorEquals.Emit(OpCodes.Callvirt, EqualityComparerEquals);

                    if(baseType != null)
                    {
                        ilgeneratorGetHashCode.Emit(OpCodes.Stloc_0);
                        ilgeneratorGetHashCode.Emit(OpCodes.Ldc_I4, -1521134295);
                        ilgeneratorGetHashCode.Emit(OpCodes.Ldloc_0);
                        ilgeneratorGetHashCode.Emit(OpCodes.Mul);
                        ilgeneratorGetHashCode.Emit(OpCodes.Ldloc_1);
                        ilgeneratorGetHashCode.Emit(OpCodes.Add);
                    }

                    // GetHashCode();
                    //MethodInfo equalityComparerTGetHashCode = TypeBuilder.GetMethod(equalityComparerT, EqualityComparerGetHashCode);
                    ilgeneratorGetHashCode.Emit(OpCodes.Stloc_0);
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldc_I4, -1521134295);
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldloc_0);
                    ilgeneratorGetHashCode.Emit(OpCodes.Mul);
                    ilgeneratorGetHashCode.Emit(OpCodes.Call, EqualityComparerDefault);
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldarg_0);
                    ilgeneratorGetHashCode.Emit(OpCodes.Ldfld, fields[i]);
                    ilgeneratorGetHashCode.Emit(OpCodes.Callvirt, EqualityComparerGetHashCode);
                    ilgeneratorGetHashCode.Emit(OpCodes.Add);
                }

                if (createParameterCtor && names.Any())
                {
                    ConstructorInfo ObjectCtor = typeof(object).GetConstructor(Type.EmptyTypes);
                    if (baseType == null)
                    {
                        // .ctor default
                        ConstructorBuilder constructorDef = typeBuilder.DefineConstructor(MethodAttributes.Public | MethodAttributes.HideBySig, CallingConventions.HasThis, Type.EmptyTypes);

                        ILGenerator ilgeneratorConstructorDef = constructorDef.GetILGenerator();
                        ilgeneratorConstructorDef.Emit(OpCodes.Ldarg_0);
                        ilgeneratorConstructorDef.Emit(OpCodes.Call, ObjectCtor);
                        ilgeneratorConstructorDef.Emit(OpCodes.Ret);
                    }
                    // .ctor with params
                    ConstructorBuilder constructor = typeBuilder.DefineConstructor(MethodAttributes.Public | MethodAttributes.HideBySig, CallingConventions.HasThis, types);

                    ILGenerator ilgeneratorConstructor = constructor.GetILGenerator();
                    ilgeneratorConstructor.Emit(OpCodes.Ldarg_0);
                    ilgeneratorConstructor.Emit(OpCodes.Call, ObjectCtor);

                    for (int i = 0; i < names.Length; i++)
                    {
                        constructor.DefineParameter(i + 1, ParameterAttributes.None, names[i]);
                        ilgeneratorConstructor.Emit(OpCodes.Ldarg_0);

                        if (i == 0)
                        {
                            ilgeneratorConstructor.Emit(OpCodes.Ldarg_1);
                        }
                        else if (i == 1)
                        {
                            ilgeneratorConstructor.Emit(OpCodes.Ldarg_2);
                        }
                        else if (i == 2)
                        {
                            ilgeneratorConstructor.Emit(OpCodes.Ldarg_3);
                        }
                        else if (i < 255)
                        {
                            ilgeneratorConstructor.Emit(OpCodes.Ldarg_S, (byte)(i + 1));
                        }
                        else
                        {
                            // Ldarg uses a ushort, but the Emit only accepts short, so we use a unchecked(...), cast to short and let the CLR interpret it as ushort.
                            ilgeneratorConstructor.Emit(OpCodes.Ldarg, unchecked((short)(i + 1)));
                        }

                        ilgeneratorConstructor.Emit(OpCodes.Stfld, fields[i]);
                    }
                    ilgeneratorConstructor.Emit(OpCodes.Ret);
                }

                if (baseType == null && names.Length == 0)
                {
                    ilgeneratorEquals.Emit(OpCodes.Ldnull);
                    ilgeneratorEquals.Emit(OpCodes.Ceq);
                    ilgeneratorEquals.Emit(OpCodes.Ldc_I4_0);
                    ilgeneratorEquals.Emit(OpCodes.Ceq);
                }
                else
                {
                    ilgeneratorEquals.Emit(OpCodes.Ret);
                    ilgeneratorEquals.MarkLabel(equalsLabel);
                    ilgeneratorEquals.Emit(OpCodes.Ldc_I4_0);
                }
            }

            if(baseType != null && propInfo == null)
            {
                ilgeneratorEquals.Emit(OpCodes.Ret);
                ilgeneratorEquals.MarkLabel(equalsLabel);
                ilgeneratorEquals.Emit(OpCodes.Ldc_I4_0);
            }

            ilgeneratorEquals.Emit(OpCodes.Ret);

            // GetHashCode()
            ilgeneratorGetHashCode.Emit(OpCodes.Stloc_0);
            ilgeneratorGetHashCode.Emit(OpCodes.Ldloc_0);
            ilgeneratorGetHashCode.Emit(OpCodes.Ret);

            return typeBuilder.CreateType();
        }

        private void EmitArgs(ILGenerator ilGenerator, object arg)
        {
            if(arg.GetType().IsArray)
            {
                int length = (int)arg.GetType().GetProperty("Length").GetValue(arg);
                var local = ilGenerator.DeclareLocal(arg.GetType());
                ilGenerator.Emit(OpCodes.Ldc_I4, length);
                ilGenerator.Emit(OpCodes.Newarr, arg.GetType().GetElementType());
                ilGenerator.Emit(OpCodes.Stloc, local);
                dynamic[] d = (dynamic[])arg;
                for(int i = 0; i < length; i++)
                {
                    ilGenerator.Emit(OpCodes.Ldloc, local);
                    ilGenerator.Emit(OpCodes.Ldc_I4, i);
                    EmitArgs(ilGenerator, d[i]);
                    ilGenerator.Emit(OpCodes.Stelem_Ref);
                }
                ilGenerator.Emit(OpCodes.Ldloc, local);
            }
            else if (arg.GetType() == typeof(int) || arg.GetType() == typeof(uint) || arg.GetType() == typeof(bool))
                ilGenerator.Emit(OpCodes.Ldc_I4, (int)arg);
            else if (arg.GetType() == typeof(long) || arg.GetType() == typeof(ulong))
                ilGenerator.Emit(OpCodes.Ldc_I8, (int)arg);
            else if (arg.GetType() == typeof(float))
                ilGenerator.Emit(OpCodes.Ldc_R4, (float)arg);
            else if (arg.GetType() == typeof(double))
                ilGenerator.Emit(OpCodes.Ldc_R8, (double)arg);
            else if (arg.GetType() == typeof(string))
                ilGenerator.Emit(OpCodes.Ldstr, (string)arg);
            else if (arg.GetType() == typeof(byte))
                ilGenerator.Emit(OpCodes.Ldc_I4_S, (byte)arg);
            else if (arg.GetType() == typeof(sbyte))
                ilGenerator.Emit(OpCodes.Ldc_I4_S, (sbyte)arg);
            else if (arg.GetType() == typeof(char))
                ilGenerator.Emit(OpCodes.Ldc_I4_S, (char)arg);
            else if (arg.GetType() == typeof(short) || arg.GetType() == typeof(ushort))
                ilGenerator.Emit(OpCodes.Ldc_I4, (short)arg);
        }

        private void EmitValueType(object arg, ILGenerator ilGenerator)
        {
            OpCode loadCode = (OpCode)typeHash[arg.GetType()];
            ilGenerator.Emit(loadCode);
        }

        //public static Type CreateType(string typeName, IEnumerable<DynamicPropertyInfo> propInfo, bool createParameterCtor = true)
        //{

        //    Type[] types = propInfo.Select(p => p.PropertyType).ToArray();
        //    string[] names = propInfo.Select(p => p.PropertyName).ToArray();

        //    Type type = moduleBuilder.GetType(typeName);
        //    if (type != null)
        //        return type;

        //    TypeBuilder tb = moduleBuilder.DefineType(typeName, TypeAttributes.AnsiClass | TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.AutoLayout | TypeAttributes.BeforeFieldInit, typeof(BusinessObjects.Specification.CATField));

        //    var fields = new FieldBuilder[names.Length];

        //    // There are two for cycles because we want to have all the getter methods before all the other methods
        //    for (int i = 0; i < names.Length; i++)
        //    {
        //        // field
        //        fields[i] = tb.DefineField($"<{names[i]}>i__Field", types[i], FieldAttributes.Private | FieldAttributes.InitOnly);

        //        PropertyBuilder property = tb.DefineProperty(names[i], PropertyAttributes.None, CallingConventions.HasThis, types[i], Type.EmptyTypes);

        //        // getter
        //        MethodBuilder getter = tb.DefineMethod($"get_{names[i]}", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName, CallingConventions.HasThis, types[i], null);
        //        //getter.SetCustomAttribute(CompilerGeneratedAttributeBuilder);
        //        ILGenerator ilgeneratorGetter = getter.GetILGenerator();
        //        ilgeneratorGetter.Emit(OpCodes.Ldarg_0);
        //        ilgeneratorGetter.Emit(OpCodes.Ldfld, fields[i]);
        //        ilgeneratorGetter.Emit(OpCodes.Ret);
        //        property.SetGetMethod(getter);

        //        // setter
        //        MethodBuilder setter = tb.DefineMethod($"set_{names[i]}", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName, CallingConventions.HasThis, null, new[] { types[i] });
        //        //setter.SetCustomAttribute(CompilerGeneratedAttributeBuilder);

        //        // workaround for https://github.com/dotnet/corefx/issues/7792
        //        setter.DefineParameter(1, ParameterAttributes.In, names[i]);

        //        ILGenerator ilgeneratorSetter = setter.GetILGenerator();
        //        ilgeneratorSetter.Emit(OpCodes.Ldarg_0);
        //        ilgeneratorSetter.Emit(OpCodes.Ldarg_1);
        //        ilgeneratorSetter.Emit(OpCodes.Stfld, fields[i]);
        //        ilgeneratorSetter.Emit(OpCodes.Ret);
        //        property.SetSetMethod(setter);
        //    }

        //    return tb.CreateType();
        //}

        //public static Type CreateType(Type baseType, params object[] baseArgs)
        //{
        //    string typeName = string.Format("{0}_{1}", baseType.Name, string.Join("_", baseArgs));

        //    Type type = moduleBuilder.GetType(typeName);
        //    if (type != null)
        //        return type;

        //    // We want to create a type that inherits baseType and calls its constructor.
        //    Type[] argsType = new Type[baseArgs.Length];
        //    int i = 0;
        //    foreach (object arg in baseArgs)
        //    {
        //        argsType[i] = arg.GetType();
        //        i++;
        //    }
        //    var baseConstructor = baseType.GetConstructor(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance, null, argsType, null);

        //    var typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.Class | TypeAttributes.Public, baseType);

        //    // Create a parameterless (default) constructor.
        //    var constructor = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, null);

        //    var ilGenerator = constructor.GetILGenerator();

        //    // Generate constructor code
        //    ilGenerator.Emit(OpCodes.Ldarg_0);                // push &quot;this&quot; onto stack.

        //    foreach (object arg in baseArgs)
        //    {
        //        GCHandle handle = GCHandle.Alloc(arg, GCHandleType.Weak);
        //        IntPtr pointer1 = GCHandle.ToIntPtr(handle);
        //        var local = ilGenerator.DeclareLocal(arg.GetType());
        //        ilGenerator.Emit(OpCodes.Ldloca, local);
        //        ilGenerator.Emit(OpCodes.Ldc_I8, pointer1.ToInt64());
        //        ilGenerator.Emit(OpCodes.Cpobj, arg.GetType());
        //        ilGenerator.Emit(OpCodes.Ldloc, local);

        //        if (arg.GetType().IsValueType)
        //        {
        //            ilGenerator.Emit(OpCodes.Unbox, arg.GetType());
        //            OpCode loadCode = (OpCode)typeHash[arg.GetType()];
        //            ilGenerator.Emit(loadCode);
        //        }
        //    }

        //    ilGenerator.Emit(OpCodes.Call, baseConstructor);  // call base constructor

        //    ilGenerator.Emit(OpCodes.Nop);                    // C# compiler add 2 NOPS, so
        //    ilGenerator.Emit(OpCodes.Nop);                    // we'll add them, too.

        //    ilGenerator.Emit(OpCodes.Ret);                    // Return

        //    Type[] convertTypes = Helper.AvailableCastType(baseType, baseType);

        //    foreach (Type convertType in convertTypes)
        //    {
        //        var methodBuilder = typeBuilder.DefineMethod("op_Implicit", MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, typeBuilder, new Type[] { convertType });

        //        var methodIlGenerator = methodBuilder.GetILGenerator();

        //        methodIlGenerator.DeclareLocal(typeBuilder);
        //        methodIlGenerator.Emit(OpCodes.Newobj, constructor);
        //        methodIlGenerator.Emit(OpCodes.Stloc_0);
        //        methodIlGenerator.Emit(OpCodes.Ldloc_0);
        //        methodIlGenerator.Emit(OpCodes.Ldarg_0);
        //        if (convertType.IsValueType)
        //        {
        //            methodIlGenerator.Emit(OpCodes.Box, convertType);
        //        }
        //        methodIlGenerator.Emit(OpCodes.Callvirt, baseType.GetMethod("set_Value", new Type[] { typeof(object) }));
        //        methodIlGenerator.Emit(OpCodes.Ldloc_0);

        //        methodIlGenerator.Emit(OpCodes.Ret);
        //    }

        //    return typeBuilder.CreateType();
        //}

    }
}
