﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Specification.DataStructure;
using BusinessObjects.Specification;
using GlobalUtils;
using BusinessObjects.Exceptions;

namespace Specification
{
    internal static class EventInstantiator
    {
        internal static CATEvent GenerateEvent(EventInfo eventInfo, SpecificationInfo specificationInfo, TypeManager typeManager)
        {
            CATEvent catEvent = null;
            try
            {
                catEvent = Activator.CreateInstance(typeManager.GetType(eventInfo.Name)) as CATEvent;
            }
            catch(Exception exp)
            {
                AppLogger.LogError(string.Format("Error occured during instantiation of {0} Event object.", eventInfo.Name));
                throw new CATEventSpecificationException(eventInfo.Name, "Error occured during instantiation of Event object. See inner exception for details.", exp);
            }

            if (catEvent != null)
            {
                foreach (var eventFieldInfo in eventInfo.Fields)
                {
                    catEvent[eventFieldInfo.Name] = GenerateField(eventFieldInfo, specificationInfo, typeManager);
                }
            }
            return catEvent;
        }

        private static ICATField GenerateField(EventFieldInfo eventFieldInfo, SpecificationInfo specificationInfo, TypeManager typeManager)
        {
            ICATField catField;
            try
            {
                var fieldInfo = specificationInfo.FieldInfos[eventFieldInfo.Name];

                object field = Activator.CreateInstance(typeManager.GetType(fieldInfo));
                catField = field as ICATField;

                if(catField == null)
                {
                    Type fieldType = field.GetType();
                    if (fieldType.IsValueType)
                    {
                        fieldType = typeof(Nullable<>).MakeGenericType(fieldType);
                    }
                    catField = Activator.CreateInstance(typeof(CATField<>).MakeGenericType(fieldType)) as ICATField;
                    catField.Value = field;
                }

                catField.Name = fieldInfo.Name;

            }
            catch (Exception exp)
            {
                AppLogger.LogError(string.Format("Error occured during initializing of {0} Field.", eventFieldInfo.Name));
                throw new CATEventSpecificationException("", eventFieldInfo.Name, "Error occured during initializing of Field. See inner exception for details.", exp);
            }

            if (catField == null)
                throw new InvalidOperationException("Cannot save non compatible Type");

            return catField;
        }
    }
}
