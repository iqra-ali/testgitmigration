﻿using GlobalUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class FieldInfo : IKey<string>
    {
        [XmlAttribute]
        public string ID { get; set; }
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String SpecializedType { get; set; }
        [XmlAttribute]
        public String DataType { get; set; }
        [XmlAttribute]
        public String TypeArgs { get; set; }
        [XmlElement("Field")]
        public List<FieldInfo> SubFields { get; set; }
        public string GetKey()
        {
            return Name;
        }
    }
}