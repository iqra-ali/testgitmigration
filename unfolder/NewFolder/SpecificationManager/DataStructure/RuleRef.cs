﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class RuleRef
    {
        [XmlAttribute]
        public string Key { get; set; }
    }
}
