﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class EventFieldInfo
    {
        [XmlAttribute]
        public int ID { get; set; }
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public EnInclude IncludeKey { get; set; }
        [XmlAttribute]
        public bool IsHardCoded { get; set; }
        [XmlAttribute]
        public String Value { get; set; }
        [XmlAttribute]
        public String EvaluateValue { get; set; }
        [XmlAttribute]
        public String DefaultValue { get; set; }
        [XmlAttribute]
        public bool Disabled { get; set; }
        [XmlAttribute]
        public String ConditionKey { get; set; }
        [XmlAttribute]
        public String RuleKey { get; set; }
    }
}