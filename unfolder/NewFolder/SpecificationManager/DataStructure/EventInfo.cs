﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class EventInfo
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }
        [XmlAttribute]
        public bool Disabled { get; set; }
        [XmlAttribute]
        public String ConditionKey { get; set; }
        [XmlAttribute]
        public String RuleKey { get; set; }

        [XmlElement("Field")]
        public List<EventFieldInfo> Fields { get; set; }
    }
}
