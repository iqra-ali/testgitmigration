﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class Event
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }

        [XmlElement("Field")]
        public List<EventField> Fields { get; set; }
    }
    public class EventField
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String DataType { get; set; }
        [XmlAttribute]
        public int MappingIndex { get; set; }

    }

    [Serializable]
    [XmlRoot("Events")]
    public class Events
    {
        [XmlArray("Events")]
        [XmlArrayItem("Event")]
        public List<Event> EventInfos { get; set; }
    }
}
