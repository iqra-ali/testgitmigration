﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class SpecializedType : GlobalUtils.IKey<string>
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string GenericType { get; set; }
        [XmlAttribute]
        public string TypeArgs { get; set; }
        //[XmlIgnore]
        //public List<FieldInfo> Fields { get; set; }
        [XmlElement("FieldProperty")]
        public List<string> FieldProperties { get; set; }

        public string GetKey()
        {
            return Name;
        }
    }
}