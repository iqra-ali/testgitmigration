﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    [Serializable]
    public class SetupAdditionalContext
    {
        [XmlElement("RulesRef")]
        public List<RuleRef> RulesRef { get; set; }
    }
}
