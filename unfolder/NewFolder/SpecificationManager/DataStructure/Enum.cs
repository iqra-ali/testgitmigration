﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Specification.DataStructure
{
    public enum EnInclude : int
    {
        [XmlEnum(Name = "R")]
        Required,
        [XmlEnum(Name = "C")]
        Conditional,
        [XmlEnum(Name = "O")]
        Optional,
        [XmlEnum(Name = "A")]
        ATS,
    }
}
