﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ConfigManager
{

    [Serializable()]
    public class File
    {
        [XmlElement("Name")]
        public string FileName { get; set; }

        [XmlElement("Path")]
        public string FilePath { get; set; }
        
        [XmlElement("Delimiter")]
        public string Delimiter { get; set; }

        [XmlElement("BoothId")]
        public string BoothId { get; set; }

        [XmlElement("ClientId")]
        public string ClientId { get; set; }

        [XmlElement("Trader")]
        public string Trader { get; set; }

        [XmlElement("ConnectionString")]
        public string ConnectionString { get; set; }

        [XmlElement("RuleEngineMappingFileName")]
        public string RuleEngineMappingFileName { get; set; }

        [XmlElement("RuleEngineMappingFilePath")]
        public string RuleEngineMappingFilePath { get; set; }

        [XmlElement("IsEnabled")]
        public bool IsEnabled { get; set; }

    }

    [Serializable()]
    [XmlRoot("FileCollection")]
    public class FileManager
    {
        [XmlArray("Files")]
        [XmlArrayItem("File", typeof(File))]
        public File[] Files { get; set; }

        //[XmlAttribute("ConnectionString")]
        //public string RuleEngineMappingFilePath { get; set; }


        public File[] DeserializeXMLConfig(string path)
        {
            FileManager files = null;
            XmlSerializer serializer = new XmlSerializer(typeof(FileManager));
            StreamReader reader = new StreamReader(path);
            files = (FileManager)serializer.Deserialize(reader);
            reader.Close();
            return files.Files;
        }
    }


}
