﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AdminRights
    {
        public int RightsId { get; set; }
        public string RightsName { get; set; }
        public string RightsDesc { get; set; }
    }
}
