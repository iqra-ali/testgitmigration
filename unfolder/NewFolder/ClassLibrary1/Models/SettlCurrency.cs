﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class SettlCurrency
    {
        public string SettlCurrencyId { get; set; }
        public string SettlCurrencyDesc { get; set; }
    }
}
