﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Nmssymbols
    {
        public string Symbol { get; set; }
        public string SymbolName { get; set; }
    }
}
