﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AldisplayPrimitive
    {
        public int DisplayPrimitiveId { get; set; }
        public string DisplayPrimitiveName { get; set; }
    }
}
