﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class DestinationsSettings
    {
        public string DestinationCode { get; set; }
        public bool? ReservedQty { get; set; }
    }
}
