﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class HandlInst
    {
        public string HandlInstId { get; set; }
        public string HandlInstDesc { get; set; }
    }
}
