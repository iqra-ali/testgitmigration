﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class GroupOrderDetails
    {
        public string GroupId { get; set; }
        public int MajorVersionNo { get; set; }
        public int MinorVersionNo { get; set; }
        public string OrderId { get; set; }
        public string BoothId { get; set; }

        public virtual GroupChange GroupChange { get; set; }
    }
}
