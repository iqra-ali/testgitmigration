﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OrderStatus
    {
        public string OrderStatus1 { get; set; }
        public string OrderStatusDesc { get; set; }
    }
}
