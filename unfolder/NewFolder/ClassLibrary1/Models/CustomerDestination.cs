﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class CustomerDestination
    {
        public string BoothId { get; set; }
        public string ClientId { get; set; }
        public string DestinationGroupId { get; set; }
        public int ActionType { get; set; }
    }
}
