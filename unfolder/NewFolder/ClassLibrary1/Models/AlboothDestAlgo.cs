﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AlboothDestAlgo
    {
        public int BoothDestAlgoId { get; set; }
        public int AlgorithmId { get; set; }
        public int AlgoId { get; set; }
        public int? GroupColCount { get; set; }
        public int? CheckBoxColCount { get; set; }
    }
}
