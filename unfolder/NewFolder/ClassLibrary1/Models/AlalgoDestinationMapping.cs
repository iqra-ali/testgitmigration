﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AlalgoDestinationMapping
    {
        public int AlgoId { get; set; }
        public string Destination { get; set; }
    }
}
