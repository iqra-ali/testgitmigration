﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Rules
    {
        public Rules()
        {
            RoleRule = new HashSet<RoleRule>();
        }

        public int RuleId { get; set; }
        public string RuleDesc { get; set; }

        public virtual ICollection<RoleRule> RoleRule { get; set; }
    }
}
