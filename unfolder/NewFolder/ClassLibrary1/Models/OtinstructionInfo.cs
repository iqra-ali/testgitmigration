﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OtinstructionInfo
    {
        public string OtinstructionId { get; set; }
        public string OtinstructionName { get; set; }
    }
}
