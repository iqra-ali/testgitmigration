﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Users
    {
        public Users()
        {
            UsersFilter = new HashSet<UsersFilter>();
        }

        public string UserId { get; set; }
        public string Pwd { get; set; }
        public string FirmId { get; set; }
        public string FirstName { get; set; }
        public string UserDesignation { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public double PriceLimit { get; set; }
        public int TotalSharesLimit { get; set; }
        public int SharesPerOrderLimit { get; set; }
        public double OptPriceLimit { get; set; }
        public int OptShrLimit { get; set; }
        public int OptOrdShrLimit { get; set; }
        public bool? IsAllocUser { get; set; }

        public virtual Firms Firm { get; set; }
        public virtual ICollection<UsersFilter> UsersFilter { get; set; }
    }
}
