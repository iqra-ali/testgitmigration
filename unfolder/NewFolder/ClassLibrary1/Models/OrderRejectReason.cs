﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OrderRejectReason
    {
        public string OrdRejReason { get; set; }
        public string OrdRejDesc { get; set; }
    }
}
