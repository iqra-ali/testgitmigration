﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Currency
    {
        public string CurrencyId { get; set; }
        public string CurrencyDesc { get; set; }
    }
}
