﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BoothDestinations
    {
        public int Seq { get; set; }
        public string BoothId { get; set; }
        public string Destination { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public bool? IsOptions { get; set; }
        public bool? IsNms { get; set; }
        public bool IsComplex { get; set; }
        public string Oatsmpid { get; set; }
        public string DestInst { get; set; }
        public string Mpid { get; set; }
        public string Oatsinst { get; set; }
        public bool IsEquityAndOption { get; set; }
        public bool? IsFutures { get; set; }
        public bool? IsInternational { get; set; }
        public string Catdest { get; set; }
        public string Catdestp { get; set; }
        public string Catsessionid { get; set; }
        public string Catexprtid { get; set; }
    }
}
