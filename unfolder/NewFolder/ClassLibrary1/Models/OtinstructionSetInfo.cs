﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OtinstructionSetInfo
    {
        public int OtinstructionSetId { get; set; }
        public string OtinstructionSetName { get; set; }
        public int GroupColCount { get; set; }
        public int CheckBoxColCount { get; set; }
        public string OtinstructionId { get; set; }
    }
}
