﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class GroupExecutions
    {
        public string GroupId { get; set; }
        public string GroupExecId { get; set; }
        public string GroupExecRefId { get; set; }
        public string Symbol { get; set; }
        public string ContraBroker { get; set; }
        public string ContraTrader { get; set; }
        public DateTime? ContraTradeTime { get; set; }
        public string LastMkt { get; set; }
        public DateTime? TradeDate { get; set; }
        public string ExecBroker { get; set; }
        public string ExecTransType { get; set; }
        public DateTime? TransactTime { get; set; }
        public string Text { get; set; }
        public string EncodedText { get; set; }
        public int? MajorVersionNo { get; set; }
        public int? MinorVersionNo { get; set; }
        public decimal? NoContraBrokers { get; set; }
        public decimal? ContraTradeQty { get; set; }
        public decimal? CumQty { get; set; }
        public decimal? DayCumQty { get; set; }
        public decimal? LastShares { get; set; }
        public decimal? LeavesQty { get; set; }
        public decimal? DayAvgPx { get; set; }
        public double? LastPx { get; set; }
        public decimal? StrikePrice { get; set; }
        public double? StopPx { get; set; }
        public double? AvgPx { get; set; }
        public double? Commission { get; set; }
        public double? Price { get; set; }
        public string SideId { get; set; }
        public string BrokerDesc { get; set; }
        public int? MessageTag { get; set; }
        public string ExecNature { get; set; }
        public string BoothId { get; set; }
        public string OrderId { get; set; }
        public string OrderExecId { get; set; }
        public string GroupIdReference { get; set; }
        public string GroupExecIdReference { get; set; }
        public string OrignatingUserId { get; set; }
    }
}
