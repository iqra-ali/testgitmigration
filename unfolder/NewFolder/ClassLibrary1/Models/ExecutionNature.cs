﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class ExecutionNature
    {
        public string ExecNature { get; set; }
        public string ExecNatureDesc { get; set; }
    }
}
