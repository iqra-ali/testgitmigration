﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class LastCapacity
    {
        public string LastCapacity1 { get; set; }
        public string LastCapacityDesc { get; set; }
    }
}
