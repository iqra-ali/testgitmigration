﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BoothUsers
    {
        public string BoothId { get; set; }
        public string UserId { get; set; }

        public virtual Booth Booth { get; set; }
    }
}
