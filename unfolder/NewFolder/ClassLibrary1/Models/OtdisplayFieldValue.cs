﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OtdisplayFieldValue
    {
        public string OtdisplayFieldValueId { get; set; }
        public string OtdisplayFieldValueName { get; set; }
        public string OtdataType { get; set; }
    }
}
