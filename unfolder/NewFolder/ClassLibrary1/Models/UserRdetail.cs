﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UserRdetail
    {
        public string UserId { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}
