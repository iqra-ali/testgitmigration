﻿using System;
using System.Runtime.Serialization;

namespace BusinessObjects.Models
{
    [Serializable]
    internal class DbEntityValidationException : Exception
    {
        private string exceptionMessage;
        private object entityValidationErrors;

        public DbEntityValidationException()
        {
        }

        public DbEntityValidationException(string message) : base(message)
        {
        }

        public DbEntityValidationException(string exceptionMessage, object entityValidationErrors)
        {
            this.exceptionMessage = exceptionMessage;
            this.entityValidationErrors = entityValidationErrors;
        }

        public DbEntityValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DbEntityValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}