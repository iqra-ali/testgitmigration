﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Role
    {
        public Role()
        {
            RoleRule = new HashSet<RoleRule>();
            UserRdetail = new HashSet<UserRdetail>();
        }

        public int RoleId { get; set; }
        public string RoleDesc { get; set; }

        public virtual ICollection<RoleRule> RoleRule { get; set; }
        public virtual ICollection<UserRdetail> UserRdetail { get; set; }
    }
}
