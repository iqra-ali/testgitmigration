﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AdminBoothList
    {
        public string BoothId { get; set; }
        public string FirmId { get; set; }
        public string BoothDescription { get; set; }
        public string Nyseroom { get; set; }
        public string BoothLocation { get; set; }
        public string OrderExecServer { get; set; }
    }
}
