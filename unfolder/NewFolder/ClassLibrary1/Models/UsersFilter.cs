﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UsersFilter
    {
        public string UserId { get; set; }
        public string BoothId { get; set; }
        public string AvailableUsers { get; set; }

        public virtual Users Users { get; set; }
    }
}
