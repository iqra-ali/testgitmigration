﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Gtbooking
    {
        public string GtbookingInst { get; set; }
        public string GtbookingDesc { get; set; }
    }
}
