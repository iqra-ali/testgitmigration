﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UserCdetail
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }

        public virtual Client Client { get; set; }
    }
}
