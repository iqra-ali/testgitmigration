﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AldisplayFieldValue
    {
        public AldisplayFieldValue()
        {
            AlalgorithmDisplayField = new HashSet<AlalgorithmDisplayField>();
        }

        public string DisplayFieldValueId { get; set; }
        public string DisplayFieldValueName { get; set; }
        public int DisplayPrimitiveId { get; set; }
        public string DataType { get; set; }
        public int? PairIdent { get; set; }

        public virtual ICollection<AlalgorithmDisplayField> AlalgorithmDisplayField { get; set; }
    }
}
