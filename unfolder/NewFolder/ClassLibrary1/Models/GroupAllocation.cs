﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class GroupAllocation
    {
        public string AllocId { get; set; }
        public string AllocGroupId { get; set; }
        public string BoothId { get; set; }
        public string AllocTransType { get; set; }
        public string PropAllocType { get; set; }
        public string RefAllocId { get; set; }
        public double? AvgPrice { get; set; }
        public string Side { get; set; }
        public string Symbol { get; set; }
        public string SymbolSfx { get; set; }
        public int? AllocShares { get; set; }
        public int? Shares { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? TransactTime { get; set; }
        public string AllocAccount { get; set; }
        public string ClientId { get; set; }
        public string CommisionType { get; set; }
        public double? Commision { get; set; }
        public int? TradeQty { get; set; }
        public string SenderSubId { get; set; }
        public string TargetSubId { get; set; }
        public string SendingComputerId { get; set; }
        public string TargetComputerId { get; set; }
        public string OnBehalfOfComputerId { get; set; }
        public string DeliverToComputerId { get; set; }
        public string SenderLocationId { get; set; }
        public string TargetLocationId { get; set; }
        public string OrignatingUserId { get; set; }
        public DateTime? SendingTime { get; set; }
        public DateTime? OrigSendingTime { get; set; }
        public string Mpid { get; set; }
        public string IsOasys { get; set; }
        public string FixAllocId { get; set; }
        public string OasysallocId { get; set; }
        public int? AccountType { get; set; }
        public string Rrnumber { get; set; }
        public string Cmta { get; set; }
        public string Dtccno { get; set; }
        public string FreeTexts { get; set; }
        public string TrailerCode { get; set; }

        public virtual GroupTrade GroupTrade { get; set; }
    }
}
