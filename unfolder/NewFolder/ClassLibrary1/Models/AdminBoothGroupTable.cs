﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AdminBoothGroupTable
    {
        public AdminBoothGroupTable()
        {
            AdminUsers = new HashSet<AdminUsers>();
        }

        public string BoothGroupId { get; set; }
        public string BoothGroupDesc { get; set; }

        public virtual ICollection<AdminUsers> AdminUsers { get; set; }
    }
}
