﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BasketDetails
    {
        public string BoothId { get; set; }
        public int BasketId { get; set; }
        public int BasketMajorVersion { get; set; }
        public int BasketMinorVersion { get; set; }
        public string OrderId { get; set; }
        public string ExtraFields { get; set; }
    }
}
