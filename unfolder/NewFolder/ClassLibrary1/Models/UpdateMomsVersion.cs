﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UpdateMomsVersion
    {
        public int VersionId { get; set; }
        public string AppVersion { get; set; }
        public string UpdateString { get; set; }
        public int? Ftpid { get; set; }
        public string Version { get; set; }

        public virtual UpdateFtpSites Ftp { get; set; }
    }
}
