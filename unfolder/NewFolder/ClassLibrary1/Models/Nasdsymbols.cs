﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Nasdsymbols
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Reserver { get; set; }
        public string MarketStat { get; set; }
        public string Test { get; set; }
        public string FinancialStatus { get; set; }
    }
}
