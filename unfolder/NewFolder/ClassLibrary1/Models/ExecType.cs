﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class ExecType
    {
        public string ExecType1 { get; set; }
        public string ExecTypeDesc { get; set; }
    }
}
