﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class RoleRule
    {
        public int RoleId { get; set; }
        public int Ruleid { get; set; }

        public virtual Role Role { get; set; }
        public virtual Rules Rule { get; set; }
    }
}
