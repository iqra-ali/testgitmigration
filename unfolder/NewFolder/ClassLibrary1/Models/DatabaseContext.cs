﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BusinessObjects.Models
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountMaster> AccountMaster { get; set; }
        public virtual DbSet<AdminBoothGroupTable> AdminBoothGroupTable { get; set; }
        public virtual DbSet<AdminBoothList> AdminBoothList { get; set; }
        public virtual DbSet<AdminRights> AdminRights { get; set; }
        public virtual DbSet<AdminRightsGroup> AdminRightsGroup { get; set; }
        public virtual DbSet<AdminRoles> AdminRoles { get; set; }
        public virtual DbSet<AdminUsers> AdminUsers { get; set; }
        public virtual DbSet<AlalgoDestinationMapping> AlalgoDestinationMapping { get; set; }
        public virtual DbSet<AlalgoTable> AlalgoTable { get; set; }
        public virtual DbSet<Alalgorithm> Alalgorithm { get; set; }
        public virtual DbSet<AlalgorithmDisplayField> AlalgorithmDisplayField { get; set; }
        public virtual DbSet<AlboothDestAlgo> AlboothDestAlgo { get; set; }
        public virtual DbSet<AldisplayField> AldisplayField { get; set; }
        public virtual DbSet<AldisplayFieldValue> AldisplayFieldValue { get; set; }
        public virtual DbSet<AldisplayPrimitive> AldisplayPrimitive { get; set; }
        public virtual DbSet<AlfieldSequence> AlfieldSequence { get; set; }
        public virtual DbSet<AlhardCodeField> AlhardCodeField { get; set; }
        public virtual DbSet<Basket> Basket { get; set; }
        public virtual DbSet<BasketDetails> BasketDetails { get; set; }
        public virtual DbSet<Booth> Booth { get; set; }
        public virtual DbSet<BoothClientsDetail> BoothClientsDetail { get; set; }
        public virtual DbSet<BoothDestinations> BoothDestinations { get; set; }
        public virtual DbSet<BoothSymbolsDetail> BoothSymbolsDetail { get; set; }
        public virtual DbSet<BoothUsers> BoothUsers { get; set; }
        public virtual DbSet<Broker> Broker { get; set; }
        public virtual DbSet<BrokerMappings> BrokerMappings { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<CommType> CommType { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<CustomerAccount> CustomerAccount { get; set; }
        public virtual DbSet<CustomerDestination> CustomerDestination { get; set; }
        public virtual DbSet<CustomerGroups> CustomerGroups { get; set; }
        public virtual DbSet<Destination> Destination { get; set; }
        public virtual DbSet<DestinationGiveUps> DestinationGiveUps { get; set; }
        public virtual DbSet<DestinationGroup> DestinationGroup { get; set; }
        public virtual DbSet<DestinationsSettings> DestinationsSettings { get; set; }
        public virtual DbSet<DiscretionInst> DiscretionInst { get; set; }
        public virtual DbSet<EtbboothCustomerGroup> EtbboothCustomerGroup { get; set; }
        public virtual DbSet<ExecInstruction> ExecInstruction { get; set; }
        public virtual DbSet<ExecRestatement> ExecRestatement { get; set; }
        public virtual DbSet<ExecTransType> ExecTransType { get; set; }
        public virtual DbSet<ExecType> ExecType { get; set; }
        public virtual DbSet<Execution> Execution { get; set; }
        public virtual DbSet<ExecutionNature> ExecutionNature { get; set; }
        public virtual DbSet<Firms> Firms { get; set; }
        public virtual DbSet<Fxrates> Fxrates { get; set; }
        public virtual DbSet<GroupAllocation> GroupAllocation { get; set; }
        public virtual DbSet<GroupChange> GroupChange { get; set; }
        public virtual DbSet<GroupExecutions> GroupExecutions { get; set; }
        public virtual DbSet<GroupOrderDetails> GroupOrderDetails { get; set; }
        public virtual DbSet<GroupTrade> GroupTrade { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Gtbooking> Gtbooking { get; set; }
        public virtual DbSet<HandlInst> HandlInst { get; set; }
        public virtual DbSet<Idsource> Idsource { get; set; }
        public virtual DbSet<LastCapacity> LastCapacity { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<MultiLegReporting> MultiLegReporting { get; set; }
        public virtual DbSet<Nasdsymbols> Nasdsymbols { get; set; }
        public virtual DbSet<Nmssymbols> Nmssymbols { get; set; }
        public virtual DbSet<Oatsautoroute> Oatsautoroute { get; set; }
        public virtual DbSet<OptAttribute> OptAttribute { get; set; }
        public virtual DbSet<OrderDetails> OrderDetails { get; set; }
        public virtual DbSet<OrderRejectReason> OrderRejectReason { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<OrderType> OrderType { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<OrfOatssymbolCategory> OrfOatssymbolCategory { get; set; }
        public virtual DbSet<OtdisplayFieldValue> OtdisplayFieldValue { get; set; }
        public virtual DbSet<OtdisplayHeadingField> OtdisplayHeadingField { get; set; }
        public virtual DbSet<OtinstructionInfo> OtinstructionInfo { get; set; }
        public virtual DbSet<OtinstructionSetInfo> OtinstructionSetInfo { get; set; }
        public virtual DbSet<ProcessCode> ProcessCode { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RoleRule> RoleRule { get; set; }
        public virtual DbSet<Rule80A> Rule80A { get; set; }
        public virtual DbSet<Rules> Rules { get; set; }
        public virtual DbSet<SecurityType> SecurityType { get; set; }
        public virtual DbSet<SettlCurrency> SettlCurrency { get; set; }
        public virtual DbSet<SettlementType> SettlementType { get; set; }
        public virtual DbSet<Side> Side { get; set; }
        public virtual DbSet<Symbol> Symbol { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<TickSizeSymbolList> TickSizeSymbolList { get; set; }
        public virtual DbSet<TimeInForce> TimeInForce { get; set; }
        public virtual DbSet<Trader> Trader { get; set; }
        public virtual DbSet<TradingSession> TradingSession { get; set; }
        public virtual DbSet<UnsolFillsReference> UnsolFillsReference { get; set; }
        public virtual DbSet<UpdateAppUserVersion> UpdateAppUserVersion { get; set; }
        public virtual DbSet<UpdateAppUsers> UpdateAppUsers { get; set; }
        public virtual DbSet<UpdateFtpSites> UpdateFtpSites { get; set; }
        public virtual DbSet<UpdateMomsVersion> UpdateMomsVersion { get; set; }
        public virtual DbSet<UserApplicationLogin> UserApplicationLogin { get; set; }
        public virtual DbSet<UserCdetail> UserCdetail { get; set; }
        public virtual DbSet<UserCustomerGroup> UserCustomerGroup { get; set; }
        public virtual DbSet<UserGroups> UserGroups { get; set; }
        public virtual DbSet<UserRdetail> UserRdetail { get; set; }
        public virtual DbSet<UserVersion> UserVersion { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersFilter> UsersFilter { get; set; }
        public virtual DbSet<Wave> Wave { get; set; }
        public virtual DbSet<WaveDetails> WaveDetails { get; set; }

        // Unable to generate entity type for table 'dbo.Reject'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ETBCustomerGroup'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BoothDestAlgo'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BoothSettings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Update_MOMS_Users'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ContraExecution'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ModOTSSymbolsList'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AdminBoothGroupRel'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OTSSymbol'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AdminRightsGroupRelTable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UserSettings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DestinationsInfo'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AlgorithmDisplayField'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Customer-OTA'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Customer-OTA-Details'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Results'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SystemSettings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SimmonTemp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SymbolTraderLimit'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OTSSymbols'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BOUploads'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PositionsBOK'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PositionsBuyBOK'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AllocInstances'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExecInstances'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PositionsSellBOK'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FixAllocation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OrderInstances'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AdminSettings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.JeffAccounts'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ControlDependency'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Algorithm'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ALControlDependency'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ALDependencyType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DestinationCrossType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DependencyType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DisplayField'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DisplayFieldValue'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DisplayPrimitive'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FieldSequence'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HardCodeField'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ETFSymbols'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OrderDiscrepancies'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OTDisplayPrimitive'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BoothSeqNo'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OTDesInstructionSet'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Customer'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OTInstructionSet'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.InternationalExchanges'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CustomerAccountDetail'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FidelityConfig'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BODRecordsNew'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FidelityRecords'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DisplayFieldValueBackup'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BoothBorrowListID'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.GarbanConfig'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BorrowList'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.GarbanRecords'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.JITETB'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.newtable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TempTable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HelfantConfig'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.temp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HoliDays'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Allocation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.GroupTradeOrderDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UnsolicitedFills'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               /*#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings*/
                optionsBuilder.UseSqlServer("Server=IONKHI-DB1601\\MSSQL2008;Database=MOMS_2.23;Persist Security Info=True;User ID=sa;Password=sa123;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<AccountMaster>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountMaster1)
                    .IsRequired()
                    .HasColumnName("AccountMaster")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdminBoothGroupTable>(entity =>
            {
                entity.HasKey(e => e.BoothGroupId);

                entity.Property(e => e.BoothGroupId)
                    .HasColumnName("BoothGroupID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BoothGroupDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdminBoothList>(entity =>
            {
                entity.HasKey(e => e.BoothId);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BoothDescription)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothLocation)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirmId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nyseroom)
                    .HasColumnName("NYSERoom")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrderExecServer)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdminRights>(entity =>
            {
                entity.HasKey(e => e.RightsId);

                entity.Property(e => e.RightsId)
                    .HasColumnName("RightsID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RightsDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RightsName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdminRightsGroup>(entity =>
            {
                entity.HasKey(e => e.RightsGroupId);

                entity.Property(e => e.RightsGroupId)
                    .HasColumnName("RightsGroupID")
                    .ValueGeneratedNever();

                entity.Property(e => e.GroupDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdminRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.Property(e => e.RoleId)
                    .HasColumnName("RoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RightsGroupId).HasColumnName("RightsGroupID");

                entity.Property(e => e.RoleDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RightsGroup)
                    .WithMany(p => p.AdminRoles)
                    .HasForeignKey(d => d.RightsGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdminRoles_AdminRightsGroup");
            });

            modelBuilder.Entity<AdminUsers>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BoothGroupId)
                    .HasColumnName("BoothGroupID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pwd)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.Telephone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserDesignation)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.BoothGroup)
                    .WithMany(p => p.AdminUsers)
                    .HasForeignKey(d => d.BoothGroupId)
                    .HasConstraintName("FK_AdminUsers_AdminBoothGroupTable");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AdminUsers)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_AdminUsers_AdminRoles");
            });

            modelBuilder.Entity<AlalgoDestinationMapping>(entity =>
            {
                entity.HasKey(e => e.AlgoId);

                entity.ToTable("ALAlgoDestinationMapping");

                entity.Property(e => e.AlgoId).ValueGeneratedNever();

                entity.Property(e => e.Destination)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AlalgoTable>(entity =>
            {
                entity.HasKey(e => e.AlgoId);

                entity.ToTable("ALAlgoTable");

                entity.Property(e => e.AlgoFixTag)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AlgoName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Alalgorithm>(entity =>
            {
                entity.HasKey(e => e.AlgorithmId);

                entity.ToTable("ALAlgorithm");

                entity.Property(e => e.AlgorithmName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsIso).HasColumnName("IsISO");
            });

            modelBuilder.Entity<AlalgorithmDisplayField>(entity =>
            {
                entity.HasKey(e => e.AlgorithmDisplayFieldId);

                entity.ToTable("ALAlgorithmDisplayField");

                entity.Property(e => e.DefaultValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisplayFieldValueId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fixtag)
                    .IsRequired()
                    .HasColumnName("FIXTag")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaxRange)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MinRange)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DisplayField)
                    .WithMany(p => p.AlalgorithmDisplayField)
                    .HasForeignKey(d => d.DisplayFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ALAlgorithmDisplayField_ALDisplayField");

                entity.HasOne(d => d.DisplayFieldValue)
                    .WithMany(p => p.AlalgorithmDisplayField)
                    .HasForeignKey(d => d.DisplayFieldValueId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ALAlgorithmDisplayField_ALDisplayFieldValue");
            });

            modelBuilder.Entity<AlboothDestAlgo>(entity =>
            {
                entity.HasKey(e => new { e.BoothDestAlgoId, e.AlgorithmId, e.AlgoId });

                entity.ToTable("ALBoothDestAlgo");

                entity.Property(e => e.BoothDestAlgoId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<AldisplayField>(entity =>
            {
                entity.HasKey(e => e.DisplayFieldId);

                entity.ToTable("ALDisplayField");

                entity.Property(e => e.DisplayFieldId).ValueGeneratedNever();

                entity.Property(e => e.DisplayFieldName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AldisplayFieldValue>(entity =>
            {
                entity.HasKey(e => e.DisplayFieldValueId);

                entity.ToTable("ALDisplayFieldValue");

                entity.Property(e => e.DisplayFieldValueId)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DataType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisplayFieldValueName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AldisplayPrimitive>(entity =>
            {
                entity.HasKey(e => e.DisplayPrimitiveId);

                entity.ToTable("ALDisplayPrimitive");

                entity.Property(e => e.DisplayPrimitiveId).ValueGeneratedNever();

                entity.Property(e => e.DisplayPrimitiveName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AlfieldSequence>(entity =>
            {
                entity.HasKey(e => e.DisplaySeqNumId);

                entity.ToTable("ALFieldSequence");
            });

            modelBuilder.Entity<AlhardCodeField>(entity =>
            {
                entity.HasKey(e => e.HardCodeFieldId);

                entity.ToTable("ALHardCodeField");

                entity.Property(e => e.Tag)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Basket>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.BasketId, e.BasketMajorVersion });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.BasketId).HasColumnName("BasketID");

                entity.Property(e => e.BasketName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BasketTransType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraFields)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OriginatingUser)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TrasactTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<BasketDetails>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.BasketId, e.BasketMajorVersion, e.OrderId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BasketId).HasColumnName("BasketID");

                entity.Property(e => e.OrderId)
                    .HasColumnName("OrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraFields)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Booth>(entity =>
            {
                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AuditReport)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.BoothDescription)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothLocation)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirmId)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Nyseroom)
                    .HasColumnName("NYSERoom")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VolumeReport)
                    .IsRequired()
                    .HasDefaultValueSql("(1)");

                entity.HasOne(d => d.Firm)
                    .WithMany(p => p.Booth)
                    .HasForeignKey(d => d.FirmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Booth_Firms");
            });

            modelBuilder.Entity<BoothClientsDetail>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.BoothId });

                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BoothDestinations>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.Destination });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Catdest)
                    .HasColumnName("CATDEST")
                    .IsUnicode(false);

                entity.Property(e => e.Catdestp)
                    .HasColumnName("CATDESTP")
                    .IsUnicode(false);

                entity.Property(e => e.Catexprtid)
                    .HasColumnName("CATEXPRTID")
                    .IsUnicode(false);

                entity.Property(e => e.Catsessionid)
                    .HasColumnName("CATSESSIONID")
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DestInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsInternational).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsNms)
                    .HasColumnName("IsNMS")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOptions).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mpid)
                    .HasColumnName("MPID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oatsinst)
                    .HasColumnName("OATSInst")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oatsmpid)
                    .HasColumnName("OATSMPID")
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BoothSymbolsDetail>(entity =>
            {
                entity.HasKey(e => new { e.Symbol, e.BoothId });

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BoothUsers>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.UserId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Booth)
                    .WithMany(p => p.BoothUsers)
                    .HasForeignKey(d => d.BoothId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BoothUsers_Booth");
            });

            modelBuilder.Entity<Broker>(entity =>
            {
                entity.Property(e => e.BrokerId)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BrokerDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Broker)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK_Broker_Client");
            });

            modelBuilder.Entity<BrokerMappings>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.Broker, e.ClearingFile });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Broker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClearingFile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Blotter)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dttc)
                    .HasColumnName("DTTC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Market)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mpid)
                    .HasColumnName("MPID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CommType>(entity =>
            {
                entity.HasKey(e => e.CommissionType);

                entity.Property(e => e.CommissionType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CommissionTypeDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.CurrencyId)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerAccount>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.ClientId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AccountType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Catacctype)
                    .HasColumnName("CATACCTYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Catdepttype)
                    .HasColumnName("CATDEPTTYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Catimid)
                    .HasColumnName("CATIMID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Catsendertype)
                    .HasColumnName("CATSENDERTYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ClientType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Cmta)
                    .HasColumnName("CMTA")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CommissionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ComplexDestination)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Cvrd)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fdid)
                    .HasColumnName("FDID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GiveUp)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Instr)
                    .HasColumnName("INSTR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsOasys)
                    .HasColumnName("IsOASYS")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IsSme).HasColumnName("IsSME");

                entity.Property(e => e.Ltid)
                    .HasColumnName("LTID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MasterAccount)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OptionDestination)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.OptionsCommissionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Range)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RepCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Stlmt)
                    .HasColumnName("STLMT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Tif)
                    .HasColumnName("TIF")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerDestination>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.ClientId, e.DestinationGroupId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DestinationGroupId)
                    .HasColumnName("DestinationGroupID")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerGroups>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.ClientId, e.GroupId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Destination>(entity =>
            {
                entity.Property(e => e.DestinationId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DestinationDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DestinationGiveUps>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.Destination, e.GiveUp });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.GiveUp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Cmtalist)
                    .HasColumnName("CMTAList")
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DestinationGroup>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.DestinationGroupId, e.Destination });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DestinationGroupId)
                    .HasColumnName("DestinationGroupID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DestinationsSettings>(entity =>
            {
                entity.HasKey(e => e.DestinationCode);

                entity.Property(e => e.DestinationCode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<DiscretionInst>(entity =>
            {
                entity.HasKey(e => e.DiscretionInst1);

                entity.Property(e => e.DiscretionInst1)
                    .HasColumnName("DiscretionInst")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DiscretionInstDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EtbboothCustomerGroup>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.GroupId })
                    .HasName("PK_ETBBoothCustomerGroup_1");

                entity.ToTable("ETBBoothCustomerGroup");

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupType).HasDefaultValueSql("(0)");

                entity.Property(e => e.ListType).HasDefaultValueSql("(0)");
            });

            modelBuilder.Entity<ExecInstruction>(entity =>
            {
                entity.HasKey(e => e.ExecInst);

                entity.Property(e => e.ExecInst)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecInstructionDesc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ExecRestatement>(entity =>
            {
                entity.HasKey(e => e.ExecRestatementReason);

                entity.Property(e => e.ExecRestatementReason)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecRestatementDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ExecTransType>(entity =>
            {
                entity.HasKey(e => e.ExecTransType1);

                entity.Property(e => e.ExecTransType1)
                    .HasColumnName("ExecTransType")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecTransTypeDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ExecType>(entity =>
            {
                entity.HasKey(e => e.ExecType1);

                entity.Property(e => e.ExecType1)
                    .HasColumnName("ExecType")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecTypeDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Execution>(entity =>
            {
                entity.HasKey(e => new { e.ExecId, e.BoothId, e.OrderId });

                entity.Property(e => e.ExecId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Account)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BaseCurrencyInfo)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerBadgeNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChangedShares).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientOrderId)
                    .HasColumnName("ClientOrderID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CommissionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContraBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContraClearingFirm)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContraTradeQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.ContraTradeTime).HasColumnType("datetime");

                entity.Property(e => e.ContraTrader)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CumQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.DeliverToComputerId)
                    .HasColumnName("DeliverToComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DropCopyFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EnteringFirm)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExecBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExecIdReference)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ExecInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExecNature)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExecRefId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExecTransType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExecType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExpireTime).HasColumnType("datetime");

                entity.Property(e => e.ExtClOrderId)
                    .HasColumnName("ExtClOrderID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ExtExecId)
                    .HasColumnName("ExtExecID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ExtOrderId)
                    .HasColumnName("ExtOrderID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraFields)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.GroupExecId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HandlInst)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Instructions)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LastMkt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastShares).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.LeavesQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.LegRefId)
                    .HasColumnName("LegRefID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LiquidityInd)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MajorBadge)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MarketData)
                    .HasMaxLength(1200)
                    .IsUnicode(false);

                entity.Property(e => e.MaxFloor).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.MaxShow).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.MemoAb)
                    .HasColumnName("MemoAB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MinQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.NoContraBrokers).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.OcscontrolNum)
                    .HasColumnName("OCSControlNum")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OnBehalfOfComputerId)
                    .HasColumnName("OnBehalfOfComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrdDestination)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrdRejDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrdRejReason)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCancelRejReason)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCapacity2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrderIdReference)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrderQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.OrderRefDate).HasColumnType("datetime");

                entity.Property(e => e.OrderStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrderType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrigCiordId)
                    .HasColumnName("OrigCIOrdID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrigSendingTime).HasColumnType("datetime");

                entity.Property(e => e.OriginalOrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrignatingUserId)
                    .HasColumnName("OrignatingUserID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ParentOrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ReportToExch).HasColumnType("numeric(1, 0)");

                entity.Property(e => e.RoutedQty).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Rule80A)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityExchange)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SenderLocationId)
                    .HasColumnName("SenderLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SenderSubId)
                    .HasColumnName("SenderSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingComputerId)
                    .HasColumnName("SendingComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingTime).HasColumnType("datetime");

                entity.Property(e => e.SettlementId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SideId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialTradeIndicator)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SymbolSfx)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TargetComputerId)
                    .HasColumnName("TargetComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetLocationId)
                    .HasColumnName("TargetLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetSubId)
                    .HasColumnName("TargetSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TimeInForceId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TransactTime).HasColumnType("datetime");

                entity.Property(e => e.TransactTimeMicroseconds)
                    .HasColumnName("TransactTime_Microseconds")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Trffields)
                    .HasColumnName("TRFFields")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Vwap).HasColumnName("VWAP");

                entity.Property(e => e.WorkableQty).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.WriteInTime).HasColumnType("datetime");

                entity.HasOne(d => d.Orders)
                    .WithMany(p => p.Executions)
                    .HasForeignKey(d => new { d.BoothId, d.OrderId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Orders_Execution");
            });

            modelBuilder.Entity<ExecutionNature>(entity =>
            {
                entity.HasKey(e => e.ExecNature);

                entity.Property(e => e.ExecNature)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecNatureDesc)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Firms>(entity =>
            {
                entity.HasKey(e => e.FirmId);

                entity.Property(e => e.FirmId)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OptOrdShrLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.OptPriceLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.OptShrLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.PriceLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.SharesPerOrderLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.TotalSharesLimit).HasDefaultValueSql("(0)");
            });

            modelBuilder.Entity<Fxrates>(entity =>
            {
                entity.HasKey(e => e.Instrument);

                entity.ToTable("FXRates");

                entity.Property(e => e.Instrument)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GroupAllocation>(entity =>
            {
                entity.HasKey(e => new { e.AllocId, e.AllocGroupId, e.BoothId });

                entity.Property(e => e.AllocId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AllocGroupId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AllocAccount)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllocTransType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Cmta)
                    .HasColumnName("CMTA")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CommisionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DeliverToComputerId)
                    .HasColumnName("DeliverToComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dtccno)
                    .HasColumnName("DTCCNo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FixAllocId)
                    .HasColumnName("FixAllocID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FreeTexts)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsOasys)
                    .HasColumnName("IsOASYS")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Mpid)
                    .HasColumnName("MPID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OasysallocId)
                    .HasColumnName("OASYSAllocID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OnBehalfOfComputerId)
                    .HasColumnName("OnBehalfOfComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrigSendingTime).HasColumnType("datetime");

                entity.Property(e => e.OrignatingUserId)
                    .HasColumnName("OrignatingUserID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PropAllocType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RefAllocId)
                    .HasColumnName("RefAllocID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Rrnumber)
                    .HasColumnName("RRNumber")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SenderLocationId)
                    .HasColumnName("SenderLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SenderSubId)
                    .HasColumnName("SenderSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingComputerId)
                    .HasColumnName("SendingComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingTime).HasColumnType("datetime");

                entity.Property(e => e.Side)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SymbolSfx)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TargetComputerId)
                    .HasColumnName("TargetComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetLocationId)
                    .HasColumnName("TargetLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetSubId)
                    .HasColumnName("TargetSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TrailerCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactTime).HasColumnType("datetime");

                entity.HasOne(d => d.GroupTrade)
                    .WithMany(p => p.GroupAllocation)
                    .HasForeignKey(d => new { d.AllocGroupId, d.BoothId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupAllocation_GroupTrade");
            });

            modelBuilder.Entity<GroupChange>(entity =>
            {
                entity.HasKey(e => new { e.GroupId, e.MinorVersionNo, e.MajorVersionNo });

                entity.Property(e => e.GroupId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CumQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.LeavesQty).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.OriginatingUserDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrignatingUserId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TimeInForceId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransactTime).HasColumnType("datetime");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupChange)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Groups_GroupChange");
            });

            modelBuilder.Entity<GroupExecutions>(entity =>
            {
                entity.HasKey(e => new { e.GroupId, e.GroupExecId });

                entity.Property(e => e.GroupId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GroupExecId)
                    .HasColumnName("GroupExecID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContraBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContraTradeQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.ContraTradeTime).HasColumnType("datetime");

                entity.Property(e => e.ContraTrader)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CumQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.DayAvgPx).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.DayCumQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.EncodedText)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExecBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExecNature)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExecTransType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GroupExecIdReference)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GroupExecRefId)
                    .HasColumnName("GroupExecRefID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GroupIdReference)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastMkt)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastShares).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.LeavesQty).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.NoContraBrokers).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.OrderExecId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrignatingUserId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SideId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StrikePrice).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TransactTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<GroupOrderDetails>(entity =>
            {
                entity.HasKey(e => new { e.GroupId, e.MajorVersionNo, e.MinorVersionNo, e.OrderId, e.BoothId })
                    .HasName("PK_GroupDetails");

                entity.Property(e => e.GroupId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.HasOne(d => d.GroupChange)
                    .WithMany(p => p.GroupOrderDetails)
                    .HasForeignKey(d => new { d.GroupId, d.MinorVersionNo, d.MajorVersionNo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupChange_GroupOrderDetail");
            });

            modelBuilder.Entity<GroupTrade>(entity =>
            {
                entity.HasKey(e => new { e.AllocGroupId, e.BoothId });

                entity.Property(e => e.AllocGroupId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AllocAccount)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Currency)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DeliverToComputerId)
                    .HasColumnName("DeliverToComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OnBehalfOfComputerId)
                    .HasColumnName("OnBehalfOfComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrigSendingTime).HasColumnType("datetime");

                entity.Property(e => e.OrignatingUserId)
                    .HasColumnName("OrignatingUserID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SecExchange)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SenderLocationId)
                    .HasColumnName("SenderLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SenderSubId)
                    .HasColumnName("SenderSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingComputerId)
                    .HasColumnName("SendingComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingTime).HasColumnType("datetime");

                entity.Property(e => e.Side)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SymbolSfx)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TargetComputerId)
                    .HasColumnName("TargetComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetLocationId)
                    .HasColumnName("TargetLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetSubId)
                    .HasColumnName("TargetSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TransactTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.Property(e => e.GroupId)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BrokerDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExecAllocStrategy)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExecBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupCreationStrategy)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrigSendingTime).HasColumnType("datetime");

                entity.Property(e => e.PriceCalcStrategy)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SendingTime).HasColumnType("datetime");

                entity.Property(e => e.SettlementId)
                    .HasColumnName("SettlementID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SideId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TradingVwap).HasColumnName("TradingVWAP");

                entity.Property(e => e.Vwap).HasColumnName("VWAP");
            });

            modelBuilder.Entity<Gtbooking>(entity =>
            {
                entity.HasKey(e => e.GtbookingInst);

                entity.ToTable("GTBooking");

                entity.Property(e => e.GtbookingInst)
                    .HasColumnName("GTBookingInst")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.GtbookingDesc)
                    .HasColumnName("GTBookingDesc")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HandlInst>(entity =>
            {
                entity.Property(e => e.HandlInstId)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.HandlInstDesc)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Idsource>(entity =>
            {
                entity.HasKey(e => e.Idsource1);

                entity.ToTable("IDSource");

                entity.Property(e => e.Idsource1)
                    .HasColumnName("IDSource")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.IdsourceDesc)
                    .HasColumnName("IDSourceDesc")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LastCapacity>(entity =>
            {
                entity.HasKey(e => e.LastCapacity1);

                entity.Property(e => e.LastCapacity1)
                    .HasColumnName("LastCapacity")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.LastCapacityDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.HasKey(e => e.MessageTag);

                entity.Property(e => e.MessageTag).ValueGeneratedNever();

                entity.Property(e => e.MessageTagDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MultiLegReporting>(entity =>
            {
                entity.HasKey(e => e.MultiLegReportingType);

                entity.Property(e => e.MultiLegReportingType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.MultiLegReportingDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Nasdsymbols>(entity =>
            {
                entity.HasKey(e => e.Symbol);

                entity.ToTable("NASDSymbols");

                entity.Property(e => e.Symbol)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Category)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FinancialStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MarketStat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reserver)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Test)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Nmssymbols>(entity =>
            {
                entity.HasKey(e => e.Symbol);

                entity.ToTable("NMSSymbols");

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SymbolName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Oatsautoroute>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.Destination });

                entity.ToTable("OATSAUTOROUTE");

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Catdest)
                    .HasColumnName("CATDEST")
                    .IsUnicode(false);

                entity.Property(e => e.Catdestp)
                    .HasColumnName("CATDESTP")
                    .IsUnicode(false);

                entity.Property(e => e.Catsessionid)
                    .HasColumnName("CATSESSIONID")
                    .IsUnicode(false);

                entity.Property(e => e.Oatsexchpartid)
                    .HasColumnName("OATSEXCHPARTID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Oatsinst)
                    .HasColumnName("OATSInst")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oatsmpid)
                    .HasColumnName("OATSMPID")
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OptAttribute>(entity =>
            {
                entity.HasKey(e => e.OptAttribute1);

                entity.Property(e => e.OptAttribute1)
                    .HasColumnName("OptAttribute")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OptAttributeDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderDetails>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.OrderId, e.OrderMajorVersionNo, e.OrderMinorVersionNo });

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Accepted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AlgoInst)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BaseCurrencyInfo)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.BillTo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerBadgeNo)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CashOrderQty).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ClientOrderId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ComplexFields)
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.ContactName)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ContraBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CrossedOrderId)
                    .HasColumnName("CrossedOrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CxlType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DeliverToComputerId)
                    .HasColumnName("DeliverToComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DestinationId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExInst)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExecInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpireTime).HasColumnType("datetime");

                entity.Property(e => e.ExtraFields)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HandlInstId)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LocateReqd).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LocationId)
                    .HasColumnName("LocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarketData)
                    .HasMaxLength(1200)
                    .IsUnicode(false);

                entity.Property(e => e.MaxFloor).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MaxShow).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MinQty).HasColumnType("numeric(19, 0)");

                entity.Property(e => e.OnBehalfOfComputerId)
                    .HasColumnName("OnBehalfOfComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OptionsFields)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OrderType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrigClOrdId)
                    .HasColumnName("OrigClOrdID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrigSendingTime).HasColumnType("datetime");

                entity.Property(e => e.OriginalOrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OriginatingUserDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrignatingUserId)
                    .HasColumnName("OrignatingUserID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PairOrderId)
                    .HasColumnName("PairOrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ParentOrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PercentageOfVolume).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.SecurityInfo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SendingComputerId)
                    .HasColumnName("SendingComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SendingTime).HasColumnType("datetime");

                entity.Property(e => e.SideId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TargetComputerId)
                    .HasColumnName("TargetComputerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeInForceId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TraderInitial)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TradingDate).HasColumnType("datetime");

                entity.Property(e => e.TransactTime).HasColumnType("datetime");

                entity.Property(e => e.TransactTimeMicroseconds)
                    .HasColumnName("TransactTime_Microseconds")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Orders)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => new { d.BoothId, d.OrderId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderDetails_Orders");
            });

            modelBuilder.Entity<OrderRejectReason>(entity =>
            {
                entity.HasKey(e => e.OrdRejReason);

                entity.Property(e => e.OrdRejReason)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OrdRejDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.HasKey(e => e.OrderStatus1);

                entity.Property(e => e.OrderStatus1)
                    .HasColumnName("OrderStatus")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OrderStatusDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderType>(entity =>
            {
                entity.HasKey(e => e.OrderType1);

                entity.Property(e => e.OrderType1)
                    .HasColumnName("OrderType")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OrderTypeDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.OrderId });

                entity.Property(e => e.BoothId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Account)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothOrdIdent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CommissionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContactName)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ExDestination)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExecBroker)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalOrderId)
                    .HasColumnName("ExternalOrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FutSettDate).HasColumnType("datetime");

                entity.Property(e => e.LocationId)
                    .HasColumnName("LocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oatsinst)
                    .HasColumnName("OATSInst")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCapacity2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProcessCode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Rule80A)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityExchange)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityInfo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SenderLocationId)
                    .HasColumnName("SenderLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SenderSubId)
                    .HasColumnName("SenderSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SettlementId)
                    .HasColumnName("SettlementID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SymbolSfx)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TargetLocationId)
                    .HasColumnName("TargetLocationID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetSubId)
                    .HasColumnName("TargetSubID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WriteInTime).HasColumnType("datetime");

                entity.HasOne(d => d.Booth)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.BoothId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Booth_Orders");
            });

            modelBuilder.Entity<OrfOatssymbolCategory>(entity =>
            {
                entity.HasKey(e => e.Category);

                entity.ToTable("ORF_OATSSymbolCategory");

                entity.Property(e => e.Category)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.MarketCenterId)
                    .HasColumnName("Market_Center_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtdisplayFieldValue>(entity =>
            {
                entity.ToTable("OTDisplayFieldValue");

                entity.Property(e => e.OtdisplayFieldValueId)
                    .HasColumnName("OTDisplayFieldValueID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OtdataType)
                    .HasColumnName("OTDataType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtdisplayFieldValueName)
                    .HasColumnName("OTDisplayFieldValueName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtdisplayHeadingField>(entity =>
            {
                entity.ToTable("OTDisplayHeadingField");

                entity.Property(e => e.OtdisplayHeadingFieldId)
                    .HasColumnName("OTDisplayHeadingFieldID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OtcheckBoxHeading)
                    .HasColumnName("OTCheckBoxHeading")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtdisplayHeadingFieldName)
                    .HasColumnName("OTDisplayHeadingFieldName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtinstructionInfo>(entity =>
            {
                entity.HasKey(e => e.OtinstructionId);

                entity.ToTable("OTInstructionInfo");

                entity.Property(e => e.OtinstructionId)
                    .HasColumnName("OTInstructionID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.OtinstructionName)
                    .HasColumnName("OTInstructionName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtinstructionSetInfo>(entity =>
            {
                entity.HasKey(e => new { e.OtinstructionSetId, e.OtinstructionId });

                entity.ToTable("OTInstructionSetInfo");

                entity.Property(e => e.OtinstructionSetId).HasColumnName("OTInstructionSetID");

                entity.Property(e => e.OtinstructionId)
                    .HasColumnName("OTInstructionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtinstructionSetName)
                    .IsRequired()
                    .HasColumnName("OTInstructionSetName")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProcessCode>(entity =>
            {
                entity.HasKey(e => e.ProcessCode1);

                entity.Property(e => e.ProcessCode1)
                    .HasColumnName("ProcessCode")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ProcessCodeDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId).ValueGeneratedNever();

                entity.Property(e => e.RoleDesc)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleRule>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.Ruleid });

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleRule)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Role_RoleRule");

                entity.HasOne(d => d.Rule)
                    .WithMany(p => p.RoleRule)
                    .HasForeignKey(d => d.Ruleid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Rules_RoleRule");
            });

            modelBuilder.Entity<Rule80A>(entity =>
            {
                entity.HasKey(e => e.Rule80A1);

                entity.Property(e => e.Rule80A1)
                    .HasColumnName("Rule80A")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Rule80Adesc)
                    .HasColumnName("Rule80ADesc")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rules>(entity =>
            {
                entity.HasKey(e => e.RuleId);

                entity.Property(e => e.RuleId)
                    .HasColumnName("RuleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RuleDesc)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SecurityType>(entity =>
            {
                entity.HasKey(e => e.SecurityType1);

                entity.Property(e => e.SecurityType1)
                    .HasColumnName("SecurityType")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SecurityTypeDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SettlCurrency>(entity =>
            {
                entity.Property(e => e.SettlCurrencyId)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SettlCurrencyDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SettlementType>(entity =>
            {
                entity.HasKey(e => e.SettlementId);

                entity.Property(e => e.SettlementId)
                    .HasColumnName("SettlementID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SettlementDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Side>(entity =>
            {
                entity.Property(e => e.SideId)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SideDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Symbol>(entity =>
            {
                entity.HasKey(e => e.Symbol1);

                entity.Property(e => e.Symbol1)
                    .HasColumnName("Symbol")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Panel)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.HasKey(e => new { e.Version, e.OrderId, e.BoothId });

                entity.Property(e => e.OrderId)
                    .HasColumnName("OrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Instruction)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OriginUser)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubOrderId)
                    .HasColumnName("SubOrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TransactTime).HasColumnType("datetime");

                entity.Property(e => e.TransactTimeMicroseconds)
                    .HasColumnName("TransactTime_Microseconds")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TickSizeSymbolList>(entity =>
            {
                entity.HasKey(e => e.TickerSymbol);

                entity.Property(e => e.TickerSymbol)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.EffectiveDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListingExchange)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TickSizePilotGroup)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TimeInForce>(entity =>
            {
                entity.Property(e => e.TimeInForceId)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.TimeInForceDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Trader>(entity =>
            {
                entity.HasKey(e => new { e.TraderId, e.ClientId });

                entity.Property(e => e.TraderId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TraderDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Trader)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Client_Trader");
            });

            modelBuilder.Entity<TradingSession>(entity =>
            {
                entity.HasKey(e => e.TradingSessionCode)
                    .HasName("PK_TradingSessionCode");

                entity.Property(e => e.TradingSessionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.RangeFrom)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RangeTo)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UnsolFillsReference>(entity =>
            {
                entity.HasKey(e => new { e.ExecId, e.UnsolRefId, e.BoothId, e.ExecTimeStamp });

                entity.Property(e => e.ExecId)
                    .HasColumnName("ExecID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnsolRefId)
                    .HasColumnName("UnsolRefID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ExecTimeStamp).HasColumnType("datetime");
            });

            modelBuilder.Entity<UpdateAppUserVersion>(entity =>
            {
                entity.HasKey(e => e.AppUserVersionId)
                    .HasName("PK_Update_App_Update_UserVersion");

                entity.ToTable("Update_App_UserVersion");

                entity.Property(e => e.AppUserVersionId).HasColumnName("AppUserVersionID");

                entity.Property(e => e.AppUpdateUserId).HasColumnName("AppUpdateUserID");

                entity.Property(e => e.ForcedUpdate)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VersionId).HasColumnName("VersionID");
            });

            modelBuilder.Entity<UpdateAppUsers>(entity =>
            {
                entity.HasKey(e => e.AppUpdateUserId)
                    .HasName("PK_AutoUpdate_User");

                entity.ToTable("Update_App_Users");

                entity.Property(e => e.AppUpdateUserId).HasColumnName("AppUpdateUserID");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.ApplicationId).HasColumnName("ApplicationID");

                entity.Property(e => e.BoothId)
                    .IsRequired()
                    .HasColumnName("BoothID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UpdateFtpSites>(entity =>
            {
                entity.HasKey(e => e.FtpId)
                    .HasName("PK_FTP_Sites");

                entity.ToTable("Update_FTP_Sites");

                entity.Property(e => e.FtpId).HasColumnName("FTP_ID");

                entity.Property(e => e.FtpIpaddress)
                    .HasColumnName("FTP_IPAddress")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.FtpName)
                    .HasColumnName("FTP_Name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FtpPort).HasColumnName("FTP_Port");

                entity.Property(e => e.FtpPwd)
                    .HasColumnName("FTP_Pwd")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FtpUserName)
                    .HasColumnName("FTP_UserName")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UpdateMomsVersion>(entity =>
            {
                entity.HasKey(e => e.VersionId)
                    .HasName("PK_MOMS_Version");

                entity.ToTable("Update_MOMS_Version");

                entity.HasIndex(e => e.Version)
                    .HasName("IX_Update_MOMS_Version")
                    .IsUnique();

                entity.Property(e => e.VersionId).HasColumnName("VersionID");

                entity.Property(e => e.AppVersion)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Ftpid).HasColumnName("FTPID");

                entity.Property(e => e.UpdateString)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.Ftp)
                    .WithMany(p => p.UpdateMomsVersion)
                    .HasForeignKey(d => d.Ftpid)
                    .HasConstraintName("FK_MOMS_Version_FTP_Sites");
            });

            modelBuilder.Entity<UserApplicationLogin>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.UserId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ApplicationId).HasColumnName("ApplicationID");

                entity.Property(e => e.LastLogin)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(((1)/(1))/(1900))");
            });

            modelBuilder.Entity<UserCdetail>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ClientId })
                    .HasName("PK_UserCDesc");

                entity.ToTable("UserCDetail");

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.UserCdetail)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Client_UserCDetail");
            });

            modelBuilder.Entity<UserCustomerGroup>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.UserId, e.GroupId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserGroups>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.FirmId, e.GroupId });

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FirmId)
                    .HasColumnName("FirmID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserRdetail>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.UserId });

                entity.ToTable("UserRDetail");

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRdetail)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Role_UserRDeta");
            });

            modelBuilder.Entity<UserVersion>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.UserId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastLogin)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(1 / 1 / 1900)");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.FirmId });

                entity.HasIndex(e => new { e.UserId, e.FirmId })
                    .HasName("IX_Users");

                entity.Property(e => e.UserId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FirmId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsAllocUser).HasDefaultValueSql("(0)");

                entity.Property(e => e.LastName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OptOrdShrLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.OptPriceLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.OptShrLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.PriceLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.Pwd)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SharesPerOrderLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.Telephone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotalSharesLimit).HasDefaultValueSql("(0)");

                entity.Property(e => e.UserDesignation)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.Firm)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.FirmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Firms");
            });

            modelBuilder.Entity<UsersFilter>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.AvailableUsers, e.BoothId });

                entity.Property(e => e.UserId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AvailableUsers)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BoothId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Users)
                    .WithMany(p => p.UsersFilter)
                    .HasForeignKey(d => new { d.UserId, d.BoothId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersFilter_UsersFilter");
            });

            modelBuilder.Entity<Wave>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.BasketId, e.WaveId, e.WaveMajorVersion });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.BasketId).HasColumnName("BasketID");

                entity.Property(e => e.WaveId).HasColumnName("WaveID");

                entity.Property(e => e.ExtraFields)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.OrderIds)
                    .HasColumnName("OrderIDs")
                    .HasMaxLength(4500)
                    .IsUnicode(false);

                entity.Property(e => e.OriginatingUser)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TrasactTime).HasColumnType("datetime");

                entity.Property(e => e.WaveName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WaveTransType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WaveDetails>(entity =>
            {
                entity.HasKey(e => new { e.BoothId, e.BasketId, e.WaveId, e.WaveMajorVersion, e.OrderId });

                entity.Property(e => e.BoothId)
                    .HasColumnName("BoothID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.BasketId).HasColumnName("BasketID");

                entity.Property(e => e.WaveId).HasColumnName("WaveID");

                entity.Property(e => e.OrderId)
                    .HasColumnName("OrderID")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
        }

        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                               || e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }

        public virtual DbSet<Orders> tb_Order { get; set; }
    }
}
