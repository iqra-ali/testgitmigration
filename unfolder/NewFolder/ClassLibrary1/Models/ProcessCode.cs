﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class ProcessCode
    {
        public string ProcessCode1 { get; set; }
        public string ProcessCodeDesc { get; set; }
    }
}
