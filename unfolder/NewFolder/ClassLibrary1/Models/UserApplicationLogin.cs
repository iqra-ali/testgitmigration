﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UserApplicationLogin
    {
        public string BoothId { get; set; }
        public string UserId { get; set; }
        public int ApplicationId { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
