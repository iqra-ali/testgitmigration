﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class EtbboothCustomerGroup
    {
        public string BoothId { get; set; }
        public string GroupId { get; set; }
        public int ListType { get; set; }
        public int GroupType { get; set; }
    }
}
