﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BoothSymbolsDetail
    {
        public string Symbol { get; set; }
        public string BoothId { get; set; }
    }
}
