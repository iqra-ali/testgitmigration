﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class CommType
    {
        public string CommissionType { get; set; }
        public string CommissionTypeDesc { get; set; }
    }
}
