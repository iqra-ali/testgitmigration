﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Firms
    {
        public Firms()
        {
            Booth = new HashSet<Booth>();
            Users = new HashSet<Users>();
        }

        public string FirmId { get; set; }
        public string Name { get; set; }
        public double PriceLimit { get; set; }
        public int TotalSharesLimit { get; set; }
        public int SharesPerOrderLimit { get; set; }
        public double OptPriceLimit { get; set; }
        public int OptShrLimit { get; set; }
        public int OptOrdShrLimit { get; set; }

        public virtual ICollection<Booth> Booth { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
