﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class DestinationGiveUps
    {
        public string BoothId { get; set; }
        public string Destination { get; set; }
        public string GiveUp { get; set; }
        public string Cmtalist { get; set; }
        public bool? IsDefault { get; set; }
    }
}
