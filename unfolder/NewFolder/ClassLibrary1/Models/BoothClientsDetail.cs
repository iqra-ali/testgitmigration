﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BoothClientsDetail
    {
        public string ClientId { get; set; }
        public string BoothId { get; set; }
    }
}
