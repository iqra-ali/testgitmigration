﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AlalgorithmDisplayField
    {
        public int AlgorithmDisplayFieldId { get; set; }
        public int BoothDestAlgoId { get; set; }
        public int DisplayFieldId { get; set; }
        public string DisplayFieldValueId { get; set; }
        public string Fixtag { get; set; }
        public string DefaultValue { get; set; }
        public string Text { get; set; }
        public int? ControlDependencyId { get; set; }
        public string MinRange { get; set; }
        public string MaxRange { get; set; }
        public bool? FieldRequired { get; set; }

        public virtual AldisplayField DisplayField { get; set; }
        public virtual AldisplayFieldValue DisplayFieldValue { get; set; }
    }
}
