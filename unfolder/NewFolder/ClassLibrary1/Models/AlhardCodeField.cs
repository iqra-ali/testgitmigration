﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AlhardCodeField
    {
        public int HardCodeFieldId { get; set; }
        public int BoothDestAlgoId { get; set; }
        public string Tag { get; set; }
        public string Value { get; set; }
    }
}
