﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class ExecInstruction
    {
        public string ExecInst { get; set; }
        public string ExecInstructionDesc { get; set; }
    }
}
