﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Execution
    {
        public string ExecId { get; set; }
        public string BoothId { get; set; }
        public string OrderId { get; set; }
        public int? MajorVersionNo { get; set; }
        public int? MinorVersionNo { get; set; }
        public string ExecRefId { get; set; }
        public string CommissionType { get; set; }
        public string OriginalOrderId { get; set; }
        public string ParentOrderId { get; set; }
        public string GroupId { get; set; }
        public string GroupExecId { get; set; }
        public string ExecNature { get; set; }
        public string OrderIdReference { get; set; }
        public string ExecIdReference { get; set; }
        public string ClientId { get; set; }
        public string Symbol { get; set; }
        public string ClientOrderId { get; set; }
        public string OrigCiordId { get; set; }
        public string ContraBroker { get; set; }
        public string ContraTrader { get; set; }
        public DateTime? ContraTradeTime { get; set; }
        public string LastMkt { get; set; }
        public DateTime? TradeDate { get; set; }
        public string ExecBroker { get; set; }
        public string Account { get; set; }
        public string SymbolSfx { get; set; }
        public DateTime? ExpireTime { get; set; }
        public string ExecInst { get; set; }
        public DateTime? TransactTime { get; set; }
        public string Text { get; set; }
        public decimal? NoContraBrokers { get; set; }
        public decimal? ContraTradeQty { get; set; }
        public decimal? CumQty { get; set; }
        public decimal? ReportToExch { get; set; }
        public decimal? LastShares { get; set; }
        public decimal? OrderQty { get; set; }
        public decimal? LeavesQty { get; set; }
        public decimal? MaxFloor { get; set; }
        public decimal? MaxShow { get; set; }
        public double? LastPx { get; set; }
        public double? StopPx { get; set; }
        public double? AvgPx { get; set; }
        public double? Commission { get; set; }
        public double? Price { get; set; }
        public string TimeInForceId { get; set; }
        public string OrderType { get; set; }
        public string SideId { get; set; }
        public string SettlementId { get; set; }
        public string ExecTransType { get; set; }
        public string OrdRejReason { get; set; }
        public string HandlInst { get; set; }
        public string Rule80A { get; set; }
        public int? MessageTag { get; set; }
        public int? PossibleResend { get; set; }
        public int? PossibleDuplicate { get; set; }
        public DateTime? SendingTime { get; set; }
        public DateTime? OrigSendingTime { get; set; }
        public string OrderStatus { get; set; }
        public string ExecType { get; set; }
        public string OrignatingUserId { get; set; }
        public DateTime? WriteInTime { get; set; }
        public int? OrderMarker { get; set; }
        public int? OrdRouteStatus { get; set; }
        public string OrdDestination { get; set; }
        public decimal? WorkableQty { get; set; }
        public decimal? RoutedQty { get; set; }
        public int? AsOfIndicator { get; set; }
        public string DropCopyFlag { get; set; }
        public string ContraClearingFirm { get; set; }
        public string EnteringFirm { get; set; }
        public DateTime? OrderRefDate { get; set; }
        public string OcscontrolNum { get; set; }
        public string MajorBadge { get; set; }
        public string SpecialTradeIndicator { get; set; }
        public string MemoAb { get; set; }
        public string Destination { get; set; }
        public string OrderCancelRejReason { get; set; }
        public string OrdRejDesc { get; set; }
        public string SenderSubId { get; set; }
        public string TargetSubId { get; set; }
        public string SendingComputerId { get; set; }
        public string TargetComputerId { get; set; }
        public string OnBehalfOfComputerId { get; set; }
        public string DeliverToComputerId { get; set; }
        public string SenderLocationId { get; set; }
        public string TargetLocationId { get; set; }
        public string ExtOrderId { get; set; }
        public string ExtClOrderId { get; set; }
        public string ExtExecId { get; set; }
        public decimal? ChangedShares { get; set; }
        public double? Vwap { get; set; }
        public string OrderCapacity2 { get; set; }
        public int? PropExecType { get; set; }
        public string BrokerBadgeNo { get; set; }
        public int? DisplayQty { get; set; }
        public decimal? MinQty { get; set; }
        public double? PegDifference { get; set; }
        public string SecurityExchange { get; set; }
        public string Trffields { get; set; }
        public string LiquidityInd { get; set; }
        public string Instructions { get; set; }
        public string LegRefId { get; set; }
        public double? Markup { get; set; }
        public string MarketData { get; set; }
        public string ExtraFields { get; set; }
        public string BaseCurrencyInfo { get; set; }
        public string TransactTimeMicroseconds { get; set; }

        public virtual Orders Orders { get; set; }
    }
}
