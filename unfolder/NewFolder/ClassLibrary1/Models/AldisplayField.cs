﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AldisplayField
    {
        public AldisplayField()
        {
            AlalgorithmDisplayField = new HashSet<AlalgorithmDisplayField>();
        }

        public int DisplayFieldId { get; set; }
        public string DisplayFieldName { get; set; }

        public virtual ICollection<AlalgorithmDisplayField> AlalgorithmDisplayField { get; set; }
    }
}
