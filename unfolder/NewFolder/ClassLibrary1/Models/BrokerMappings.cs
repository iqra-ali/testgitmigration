﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class BrokerMappings
    {
        public string BoothId { get; set; }
        public string Broker { get; set; }
        public string Market { get; set; }
        public string Blotter { get; set; }
        public string Mpid { get; set; }
        public string Dttc { get; set; }
        public string ClearingFile { get; set; }
    }
}
