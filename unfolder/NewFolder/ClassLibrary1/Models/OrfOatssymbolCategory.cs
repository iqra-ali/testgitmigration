﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OrfOatssymbolCategory
    {
        public string Category { get; set; }
        public string MarketCenterId { get; set; }
    }
}
