﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Oatsautoroute
    {
        public string BoothId { get; set; }
        public string Destination { get; set; }
        public string Oatsmpid { get; set; }
        public string Oatsinst { get; set; }
        public string Oatsexchpartid { get; set; }
        public string Catdest { get; set; }
        public string Catdestp { get; set; }
        public string Catsessionid { get; set; }
    }
}
