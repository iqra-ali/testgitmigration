﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Groups
    {
        public Groups()
        {
            GroupChange = new HashSet<GroupChange>();
        }

        public string GroupId { get; set; }
        public string ExecBroker { get; set; }
        public string Symbol { get; set; }
        public double? Wap { get; set; }
        public double? Vwap { get; set; }
        public double? TradingVwap { get; set; }
        public string SettlementId { get; set; }
        public string SideId { get; set; }
        public string PriceCalcStrategy { get; set; }
        public string ExecAllocStrategy { get; set; }
        public string GroupCreationStrategy { get; set; }
        public string BrokerDesc { get; set; }
        public DateTime? SendingTime { get; set; }
        public DateTime? OrigSendingTime { get; set; }

        public virtual ICollection<GroupChange> GroupChange { get; set; }
    }
}
