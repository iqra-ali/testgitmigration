﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Destination
    {
        public string DestinationId { get; set; }
        public string DestinationDesc { get; set; }
    }
}
