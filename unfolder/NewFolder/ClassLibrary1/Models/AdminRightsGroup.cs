﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AdminRightsGroup
    {
        public AdminRightsGroup()
        {
            AdminRoles = new HashSet<AdminRoles>();
        }

        public int RightsGroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }

        public virtual ICollection<AdminRoles> AdminRoles { get; set; }
    }
}
