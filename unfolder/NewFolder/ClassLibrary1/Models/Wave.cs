﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Wave
    {
        public string BoothId { get; set; }
        public int BasketId { get; set; }
        public int WaveId { get; set; }
        public int WaveMajorVersion { get; set; }
        public int? WaveMinorVersion { get; set; }
        public string WaveName { get; set; }
        public string WaveTransType { get; set; }
        public string ExtraFields { get; set; }
        public string OrderIds { get; set; }
        public DateTime? TrasactTime { get; set; }
        public string OriginatingUser { get; set; }
        public int OperationId { get; set; }
    }
}
