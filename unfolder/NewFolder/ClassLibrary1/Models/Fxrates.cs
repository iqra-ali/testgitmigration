﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class Fxrates
    {
        public string Instrument { get; set; }
        public double? BidPrice { get; set; }
        public double? AskPrice { get; set; }
        public double? MidPrice { get; set; }
        public string DateTime { get; set; }
    }
}
