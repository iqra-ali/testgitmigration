﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OrderDetails
    {
        public string BoothId { get; set; }
        public string OrderId { get; set; }
        public int OrderMajorVersionNo { get; set; }
        public int OrderMinorVersionNo { get; set; }
        public DateTime? ExpireTime { get; set; }
        public int? OrderQty { get; set; }
        public decimal? MinQty { get; set; }
        public string OrderType { get; set; }
        public decimal? MaxFloor { get; set; }
        public double? Price { get; set; }
        public double? StopPx { get; set; }
        public string HandlInstId { get; set; }
        public double? PegDifference { get; set; }
        public decimal? MaxShow { get; set; }
        public string TimeInForceId { get; set; }
        public decimal? CashOrderQty { get; set; }
        public decimal? LocateReqd { get; set; }
        public int MessageTag { get; set; }
        public DateTime? TransactTime { get; set; }
        public int? PossibleResend { get; set; }
        public int? PossibleDuplicate { get; set; }
        public DateTime? SendingTime { get; set; }
        public DateTime? OrigSendingTime { get; set; }
        public string SideId { get; set; }
        public string OrignatingUserId { get; set; }
        public string BillTo { get; set; }
        public string BrokerBadgeNo { get; set; }
        public int? RemainingQty { get; set; }
        public string ContraBroker { get; set; }
        public int? OrdRouteStatus { get; set; }
        public decimal? PercentageOfVolume { get; set; }
        public string SendingComputerId { get; set; }
        public string TargetComputerId { get; set; }
        public string OnBehalfOfComputerId { get; set; }
        public string DeliverToComputerId { get; set; }
        public string ClientOrderId { get; set; }
        public string OriginatingUserDesc { get; set; }
        public string OrigClOrdId { get; set; }
        public int? DisplayQty { get; set; }
        public string CxlType { get; set; }
        public string ExecInst { get; set; }
        public string OriginalOrderId { get; set; }
        public string ParentOrderId { get; set; }
        public string DestinationId { get; set; }
        public int? OrderMarker { get; set; }
        public string Accepted { get; set; }
        public string ExInst { get; set; }
        public string AlgoInst { get; set; }
        public string OptionsFields { get; set; }
        public string ComplexFields { get; set; }
        public string CrossedOrderId { get; set; }
        public string ExtraFields { get; set; }
        public string PairOrderId { get; set; }
        public string TraderInitial { get; set; }
        public string MarketData { get; set; }
        public DateTime? TradingDate { get; set; }
        public string LocationId { get; set; }
        public string ContactName { get; set; }
        public string Text { get; set; }
        public string SecurityInfo { get; set; }
        public string BaseCurrencyInfo { get; set; }
        public string TransactTimeMicroseconds { get; set; }

        public virtual Orders Orders { get; set; }
    }
}
