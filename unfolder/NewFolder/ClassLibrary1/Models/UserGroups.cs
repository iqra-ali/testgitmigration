﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UserGroups
    {
        public string UserId { get; set; }
        public string FirmId { get; set; }
        public string GroupId { get; set; }
    }
}
