﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class UpdateAppUserVersion
    {
        public int AppUserVersionId { get; set; }
        public int AppUpdateUserId { get; set; }
        public int VersionId { get; set; }
        public bool? ForcedUpdate { get; set; }
    }
}
