﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.Helper_Classes
{
    public class EX : ITran
    {
        public string RecordType { get; set; }
        public string FirmorderID { get; set; }
        public string OrderReceivedDate { get; set; }
        public string Symbol { get; set; }
        public string SymbolSuffix { get; set; }
        protected decimal ExecutionQuantity { get; set; }
        protected double ExecutionPrice { get; set; }
        public string LeavesQuantity { get; set; }
        public string ExecutionTimestamp { get; set; }
        public string BranchSequenceNumber { get; set; }
        public string TraderTerminalID { get; set; }
        public string ExecutionTypeIndicator { get; set; }
        public string MarketCenterID { get; set; }
        public string CapacityCode { get; set; }
        public string ReportingExceptionCode { get; set; }
        public string FutureUse_1 { get; set; }
        public string ROEMemo { get; set; }

        public decimal Quantity { get => ExecutionQuantity; set => ExecutionQuantity = value; }
        public double Price { get => ExecutionPrice; set => ExecutionPrice = value; }


        public EX()
        {

        }

        public EX(string[] fields)
        {
                int _index = 0;
                RecordType = fields[_index];
                FirmorderID = fields[++_index];
                OrderReceivedDate = fields[++_index];
                Symbol = fields[++_index];
                SymbolSuffix = fields[++_index];
                Quantity = decimal.Parse(fields[++_index]);
                Price = double.Parse(fields[++_index]);
                LeavesQuantity = fields[++_index];
                ExecutionTimestamp = fields[++_index];
                BranchSequenceNumber = fields[++_index];
                TraderTerminalID = fields[++_index];
                ExecutionTypeIndicator = fields[++_index];
                MarketCenterID = fields[++_index];
                CapacityCode = fields[++_index];
                ReportingExceptionCode = fields[++_index];
                FutureUse_1 = fields[++_index];
                ROEMemo = fields[++_index];
        }

    }
}

