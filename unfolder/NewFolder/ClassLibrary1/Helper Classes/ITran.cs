﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.Helper_Classes
{
    public interface ITran
    {
         string RecordType { get; set; }
         string FirmorderID { get; set; }
         string Symbol { get; set; }
         decimal Quantity { get; set; }
         double Price { get; set; }
         string ROEMemo { get; set; }
         //object FillFields(string [] fields);
    }
}
