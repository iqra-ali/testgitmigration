﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObjects.Helper_Classes
{
    public class ParentEvent
    {
        public int MajorVersion = 0;
        public readonly SortedList<int, ITran> srLsttransactions = null;

        public ParentEvent(string [] fileData)
        {

        }

        public void FillTransaction(string[] fileData, Type transaction)
        {
            EX rt = new EX(fileData);
            this.srLsttransactions.Add(MajorVersion++, rt);
        }
    }
}
