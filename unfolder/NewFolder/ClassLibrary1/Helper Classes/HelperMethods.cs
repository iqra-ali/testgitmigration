﻿using BusinessObjects.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Text;

namespace BusinessObjects.Helper_Classes
{
    public static class Helper
    {
        public static List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map)
        {
            using (var context = new DatabaseContext())
            {
               context.Database.OpenConnection();
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;


                    using (var result = command.ExecuteReader())
                    {
                        var entities = new List<T>();
                        if (result.HasRows)
                        {
                            //if (!result.IsDBNull(1))
                            {
                                while (result.Read())
                                {
                                    entities.Add(map(result));
                                }
                            }
                        }
                        return entities;
                    }
                }
            }
        }

    
        public static string GetIcreamentedOrderId(string BID)
        {
            int id = int.Parse(BID);
            return (++id).ToString();
        }
        public static string GetNextOrderIdByBooth(string BID)
        {
            int inLastOrderId = -1;
            var result = Helper.RawSqlQuery(
                "SELECT isnull(MAX(convert(int,OrderId)),0) as OrderId FROM Orders where BoothId = 'MXA'",
                x => inLastOrderId = (int)x[0]);
            return (++inLastOrderId).ToString();
        }
        public static DateTime ConvertPlainStringToDatetime(string Date, string outputFormat)
        {
            DateTime date;
            //string DemoLimit = "20190204";
            string pattern = "yyyyMMddHHmmss";    // 20190204134732;

            CultureInfo enUS = new CultureInfo("en-US");
            DateTime.TryParseExact(Date, pattern, enUS,
                                DateTimeStyles.AdjustToUniversal, out date);

            string formatedDateTime = date.ToString(outputFormat);
            return Convert.ToDateTime(formatedDateTime);
            //Console.WriteLine(datelimit.ToString("yyyy-MM-dd"));
        }
    }
}
